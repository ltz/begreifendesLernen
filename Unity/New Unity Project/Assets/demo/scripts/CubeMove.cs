﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMove : MonoBehaviour {
    public Vector3 lastposition;
    //public bool hasMoved = false;
 
	// Use this for initialization
	void Start () {
        lastposition = transform.position;
       
	}

    private void TestAllResponds(int i)
    {
        Debug.Log(name + " recived " + i);
    }

    void setPosition()
    {
        if ( (Mathf.Abs(lastposition.x - transform.position.x) > 0.3 ) || (Mathf.Abs(lastposition.z - transform.position.z) > 0.3) ) {
            lastposition = transform.position;
        } 
    }
	// Update is called once per frame
	void Update () {

        float delta = 0.3f;
        float x = transform.position.x;
        float y = transform.position.y;
        float z = transform.position.z;
        if (Input.GetAxis("Horizontal") < 0)
        {
            x -= delta;
         
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            x += delta;
            
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            z -= delta;
          
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            z += delta;
         
        }
        setPosition();
        transform.position = new Vector3(x, y, z);
    }
}
