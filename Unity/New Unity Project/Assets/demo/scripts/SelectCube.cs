﻿
using UnityEngine;




public class SelectCube : MonoBehaviour
{

    public GameObject[] ob;
    public int selected = 0;
    private Color defaultColor;
   




    // Use this for initialization
    void Start()
    {
      
       
        ob[selected].gameObject.GetComponent<CubeMove>().enabled = true;
        defaultColor = ob[selected].gameObject.GetComponent<Renderer>().material.color;
        ob[selected].gameObject.GetComponent<Renderer>().material.color = Color.green;
        bool incoming = true;
          
        ob[selected].gameObject.GetComponent<Renderer>().material.color = Color.green;

        }



        // Update is called once per frame
        void Update()
    {
        

        if (Input.GetButtonDown("n"))
        {
            ob[selected].gameObject.GetComponent<CubeMove>().enabled = false;
            ob[selected].gameObject.GetComponent<Renderer>().material.color = defaultColor;
            selected = (selected + 1) % ob.Length;
            ob[selected].gameObject.GetComponent<CubeMove>().enabled = true;
            ob[selected].gameObject.GetComponent<Renderer>().material.color = Color.green;

           
        }
        if (Input.GetButtonDown("k"))
        {
            EventManagerZeroArg.TriggerEvent("DebugButtonID");
        }
        
    }
}
   
