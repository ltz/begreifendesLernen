﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ring : A_GuiElement {
    public Text desc;
    public Image ring;
    

    public override void Setup(Action action)
    {
        transform.localPosition = action.positon;
                desc.text = action.text;
                ring.color = action.color;
                // get the regColor;
                GetComponent<RingIsActivated>().regColor = action.buttonID;
            StartupAnimation();
    
    }

    public override void StartupAnimation()
    {
        GetComponent<Animation>().Play("Spawn");
       
    }
}
