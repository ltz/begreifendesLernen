﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APPConfig;

public class RadialMenu : A_GUILayer
{


    public GameObject buttonPrefab;
    private List<GameObject> buttons = new List<GameObject>();
    
    // Use this for initialization
    public override void Setup(Action[] options)
    {
      //Debug.Log("in Radialmenu options.length : " + options.Length);

        // Spawn all Elements
        for (int i = 0; i < options.Length; i++)
        {
            var button = Instantiate(buttonPrefab);
            //Debug.Log("buttonName: " + button.name);

            button.transform.SetParent(transform, false);
            // set startPosition

            options[i].positon = new Vector3(-1f, 0f, 0f) * (int) MenuSize.small;
            options[i].buttonID = i;
            button.GetComponent<RadialButton>().Setup(options[i]);
            buttons.Add(button);
        }
        // Animation: rotate all elements into Position
            StartCoroutine(AnimSpawn());
    }

    public IEnumerator AnimSpawn()
    {
        const int AnimLength = 30;
        float thetaMax = Mathf.Rad2Deg * (2 * Mathf.PI / buttons.Count); // degree
        float thetaStep = thetaMax / AnimLength;  // degree
        float theta = 0;

        for (int i = AnimLength; i >= 0; i--)
        {
            for (int j = 0; j < buttons.Count; j++)
            {
                float sigma = Mathf.Deg2Rad * (theta * j - 90); // Radiant
                                                                //Debug.Log("Sigma von " + j + " ist " + Mathf.Rad2Deg * sigma);
                float xPos = Mathf.Sin(sigma);
                float yPos = Mathf.Cos(sigma);

                buttons[j].transform.localPosition = new Vector3(xPos, yPos, 0f) * (int) MenuSize.small;
            }
            theta += thetaStep;
            yield return new WaitForEndOfFrame();
        }

    }
    public void AnimDestroy()
    {

    }

    public override void StartupAnimation()
    {
        foreach (var child in buttons)
        {
            child.GetComponent<RadialButton>().StartupAnimation();   
        }
        StartCoroutine(AnimSpawn());
    }

    public override void Remove()
    {
        throw new System.NotImplementedException();
    }
}
