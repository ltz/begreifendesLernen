﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Video: MonoBehaviour
{
    public int fps = 15; // anzahl der sprites / original length in seconds
    public string folder;
    public Image img;
    private int index = 0;
    private int frame = 0;
    private bool play;
    private AudioSource sound;

    private Sprite[] sprite = null;
    private void Start()
    {
        play = true;
        sound = GetComponent<AudioSource>();
        sound.clip = Resources.Load<AudioClip>("audio/watersound");

        if ("" != folder)
        {
            sprite = Resources.LoadAll<Sprite>(folder);
            img.sprite = sprite[index++];
        }
       
    }

  

    private int addMod(int a, int m)
    {

        return (++a) % m;
    }
    // Update is called once per frame
    void Update()
    {
        int wait = (60 / fps);
        if (frame == 0 && sprite != null)
        {


            this.img.sprite = sprite[index];
            index = addMod(index, sprite.Length);
        }
        frame = addMod(frame, wait);
        if (play)
        {
            sound.Play();
            play = false; // loops
        }
    }
}
