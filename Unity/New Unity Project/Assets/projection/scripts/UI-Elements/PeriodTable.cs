﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PeriodTable : MonoBehaviour
{

   
    private void OnEnable()
    {
        EventManagerZeroArg.StartListening("Found", PlayAnimation);
        EventManagerOneArg.StartListening("Start", HighlightColumn);
        EventManagerOneArg.StartListening("ChooseElement", HighlightElement);
        EventManagerZeroArg.StartListening("ResetColumnColor", ResetColumnsColor);
        EventManagerZeroArg.StartListening("ResetElementColor", ResetElementsColor);
        EventManagerZeroArg.StartListening("ShowRemainElements", ShowRemainElements);
        EventManagerZeroArg.StartListening("RevertRemainElements", RevertRemainElements);
    }
    private void OnDisable()
    {
        EventManagerZeroArg.StopListening("Found", PlayAnimation);
        EventManagerOneArg.StopListening("Start", HighlightColumn);
        EventManagerOneArg.StopListening("ChooseElement", HighlightElement);
        EventManagerZeroArg.StopListening("ResetColumnColor", ResetColumnsColor);
        EventManagerZeroArg.StopListening("ResetElementColor", ResetElementsColor);
        EventManagerZeroArg.StopListening("ShowRemainElements", ShowRemainElements);
        EventManagerZeroArg.StopListening("RevertRemainElements", RevertRemainElements);
    }

    private const int Pw = 18; // columns of periodtable
    private const int Ew = 90; // Elementwidth
    public  int highlightedColumn = -1;
    public GameObject goTo;
  
    private int[] columnsselect = new int[] { 0, 1, 12, 13, 14, 15, 16 };
    private string[][] pts = {
    new string[] { "H", "Li", "Na", "K", "Rb","Cs", "Fr" },
    new string[] {"Be", "Mg", "Ca", "Sr", "Ba", "Ra" },
    new string[] {"Sc", "Y", "La", "Ac" },
    new string[] {"Ti", "Zr", "Hf", "Rf" },
    new string[] {"V", "Nb", "Ta", "Db" },
    new string[] {"Cr", "Mo", "W", "Sg" },
    new string[] {"Mn", "Tc", "Re", "Bh" },
    new string[] {"Fe", "Ru", "Os", "Hs" },
    new string[] {"Co", "Rh", "Ir", "Mt" },
    new string[] {"Ni", "Pd", "Pt", "Ds" },
    new string[] {"Cu", "Ag", "Au", "Rg" },
    new string[] {"Zn", "Cd", "Hg", "Cn" },
    new string[] {"B", "Al", "Ga", "In", "Ti", "Uut" },
    new string[] {"C", "Si", "Ge", "Sn", "Pb", "Uuq" },
    new string[] {"N", "P", "As", "Sb", "Bi", "UUp" },
    new string[] {"O", "S", "Se", "Te", "Po", "Uuh" },
    new string[] {"F", "Cl", "Br", "I", "At", "Uus" },
    new string[] {"He", "Ne", "Ar", "Kr", "Xe", "Rn", "Uuo" }
    };

    private string[][] ptf = new string[Pw][] {
    new string[] { "Wasserstoff", "Li", "Na", "K", "Rb","Cs", "Fr" },
   new string[] {"Be", "Mg", "Ca", "Sr", "Ba", "Ra" },
   new string[] {"Sc", "Y", "La", "Ac" },
   new string[] {"Ti", "Zr", "Hf", "Rf" },
    new string[] {"V", "Nb", "Ta", "Db" },
    new string[] {"Cr", "Mo", "W", "Sg" },
    new string[] {"Mn", "Tc", "Re", "Bh" },
    new string[] {"Fe", "Ru", "Os", "Hs" },
    new string[] {"Co", "Rh", "Ir", "Mt" },
    new string[] {"Ni", "Pd", "Pt", "Ds" },
    new string[] {"Cu", "Ag", "Au", "Rg" },
    new string[] {"Zn", "Cd", "Hg", "Cn" },
    new string[] {"B", "Al", "Ga", "In", "Ti", "Uut" },
    new string[] {"C", "Si", "Ge", "Sn", "Pb", "Uuq" },
    new string[] {"N", "P", "As", "Sb", "Bi", "UUp" },
    new string[] {"Sauerstoff", "S", "Se", "Te", "Po", "Uuh" },
    new string[] {"F", "Cl", "Br", "I", "At", "Uus" },
    new string[] {"He", "Ne", "Ar", "Kr", "Xe", "Rn", "Uuo" }
    };
    //// Colors
    //static Color darkblue = new Color(45.0f / 255, 53f / 255, 71f / 255, 1);
    //static Color orange = new Color(221f / 255, 95f / 255, 38f / 255, 1);
    //static Color green = new Color(128f / 255, 182f / 255, 146f / 255, 1);
    //static Color pink = new Color(175f / 255, 56f / 255, 93f / 255, 1);
    //static Color yellow = new Color(244f / 255, 190f / 255, 36f / 255, 1);
    //static Color red = new Color(140f / 255, 23f / 255, 43f / 255, 1);
    //static Color cyan = new Color(84f / 255, 170f / 255, 198f / 255, 1);
    //static Color white = new Color(227f / 255, 224f / 255, 210f / 255, 1);

    Color[] colors =  new Color[Pw] {
        DATAContainer.cyan,
        DATAContainer.red,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.white,
        DATAContainer.green,
        DATAContainer.pink,
        DATAContainer.yellow,
        DATAContainer.orange,
        DATAContainer.darkblue,
        DATAContainer.white };

    GameObject[][] pt = {
        new GameObject[7],
        new GameObject[6],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[4],
        new GameObject[6],
        new GameObject[6],
        new GameObject[6],
        new GameObject[6],
        new GameObject[6],
        new GameObject[7]
    };

    public GameObject prefab ;

    // Use this for initialization
     void Start()
    {
      EventManagerZeroArg.StartListening("DebugButtonID", PlayAnimation);
        int pos_x = 0;

        for (int i = 0; i < Pw; i++) // foreach column
        {

            int pos_y = 0;
            pos_x = Ew * i; // set the offset;
            Color columnColor = colors[i];
            for (int j = pts[i].Length - 1; j >= 0; j--) // Setup SData
            {
                int forwardIndex = (pts[i].Length - (1 + j));
                pos_y = forwardIndex * 90;

                var element = Instantiate(prefab);

                element.GetComponent<RectTransform>().SetParent(transform, false);
                element.GetComponent<RectTransform>().localPosition = new Vector3(pos_x, pos_y, 0);
                element.GetComponent<Image>().color = columnColor;

                element.GetComponentInChildren<Text>().text = pts[i][j];
                pt[i][forwardIndex] = element;


            }
//_columns[i] = new Column(pts[i].Length);
        }

    }

    public int GetElemntsInColumn(int i)
    {
        return pts[i].Length;
    }
    public string[] GetColumnTexts(int i)
    {
        return pts[i];
    }
    public string[] GetColumnFullTexts(int i) 
    {
      return ptf[i];
    }


    public void PlayAnimation() {
       GetComponent<Animation>().Play("small");
       EventManagerOneArg.TriggerEvent("nextState", goTo);
     }
     
    public void HighlightColumn(GameObject index)
    {
        highlightedColumn = columnsselect[index.GetComponent<RadialButton>().buttonID];
        
        for (int i = 0; i < Pw; i++)
        {
            if (highlightedColumn == i)
            {
                continue;
            }
            for (int j = 0; j < pt[i].Length; j++)
            {
                pt[i][j].GetComponent<Image>().color = DATAContainer.white;
            }
        }
    }

    public void HighlightElement(GameObject index)
    {
        var element = index.GetComponent<RadialButton>().buttonID;
       
        for (int i = 0; i < pt[highlightedColumn].Length; i++)
        {
            var forwardIndex = (pts[highlightedColumn].Length - (1 + i));
            if (element != i)
            {
                pt[highlightedColumn][forwardIndex].GetComponent<Image>().color = DATAContainer.white;
                
                
            }
             
        }
        Debug.Log("choosen");
        EventManagerZeroArg.TriggerEvent("choosen");
    }

    // Reset all Colors of one Column
    void ResetElementsColor() {
      for (int i = 0; i < pt[highlightedColumn].Length; i++)
      {
        pt[highlightedColumn][i].GetComponent<Image>().color = colors[highlightedColumn];
      }
    }

    // Resets all Colors in all Columns
    void ResetColumnsColor() {
      for  ( int i = 0 ; i < pt.Length ; i++) {
        var col = pt[i];
        foreach( var elm in col) {
          elm.GetComponent<Image>().color = colors[i];
        }
      }
    }

    // show the remaining Elements
    void ShowRemainElements() {
      pt[0][6].GetComponent<Image>().color = colors[0];
      pt[15][5].GetComponent<Image>().color = colors[15];
    }
    void RevertRemainElements() {
      // all Columns which are NOT highlightedColumn need revert 
        for (int i = 0; i < Pw; i++)
        {
            if (highlightedColumn == i)
            {
                continue;
            }
            for (int j = 0; j < pt[i].Length; j++)
            {
                pt[i][j].GetComponent<Image>().color = DATAContainer.white;
            }
        }
    }
}
