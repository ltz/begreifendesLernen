﻿using UnityEngine;
public abstract class A_GuiElement : MonoBehaviour
{ 
 
    public abstract void Setup(Action action);
    public abstract void StartupAnimation();

    
     public void Remove()
    {
        GetComponent<Animation>().Play("Destroy");
        Destroy(gameObject, GetComponent<Animation>().GetClip("Destroy").length);

    }
    }
