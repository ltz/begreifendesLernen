﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using APPConfig;
using System;


public class RadialButton : A_GuiElement
{
    // GUI
    public Image loading;
    public Image background;
   
    
    public Text Desc;
    public Text FullText;

    // Trigger
    public int spawntime;
    public bool spawnTriggered = false;
    public Interaction goToState;


    // Events
    public int buttonID = -1;
    public ButtonType buttontype ;

    private void OnEnable() {
      EventManagerZeroArg.StartListening("ShowFullText", ShowFullText);
      EventManagerZeroArg.StartListening("HideFullText", HideFullText);
    }
    private void OnDisable() {
      EventManagerZeroArg.StopListening("ShowFullText", ShowFullText);
      EventManagerZeroArg.StopListening("HideFullText", HideFullText);
    }

   
        
    public override void Setup(Action opt)
    {

        transform.localPosition = opt.positon;

        loading.fillAmount = 0;
        
        //icon.sprite = opt.sprite;
        Desc.text = opt.text;
        goToState = opt.goToState;
        buttontype = opt.buttontype;
          if (ButtonType.Inactive == buttontype) {
            background.color = DATAContainer.gray;
          }

        buttonID = opt.buttonID;

        FullText.GetComponent<Transform>().localPosition = FullText.GetComponent<Transform>().localPosition / (float) MenuSize.small * (float) MenuSize.large;
        FullText.text = opt.fullText;
        FullText.gameObject.SetActive(false);
        StartupAnimation();

    }

    public override void StartupAnimation()
    {
        GetComponent<Animation>().Play("Spawn");

        Invoke("TriggerEnable", GetComponent<Animation>().GetClip("Spawn").length);

    }
    private void TriggerEnable()
    {
        spawntime = Time.frameCount;
        if (ButtonType.Inactive != buttontype) {
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
        }

    }

    private void ShowFullText() {
      FullText.gameObject.SetActive(true);
    }

    private void HideFullText() {
      FullText.gameObject.SetActive(false);
    }



}
