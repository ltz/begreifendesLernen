﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingLayout : A_GUILayer
{
    public Ring ringPrefab;
    
    private List<Ring> rings = new List<Ring>();
    
    private int offset_Y = -200;
    private const int midDistance = 300;

    private int count;
    public int triggert;
    public Interaction goToState;
   
    
    public override void Setup(Action[] options)
    {
       
        count = options.Length; 
        triggert = 0;

        // TODO make offset_Y for more than 3 Elements
        // Spawn all Rings
        for (int i = 0; i < options.Length; i++)
        {
           
            Ring ring = Instantiate(ringPrefab);
            ring.transform.SetParent(transform, false);
            options[i].positon  = new Vector3Int(0, 0, 0);
            ring.Setup(options[i]);
            rings.Add(ring); 
        }
            StartCoroutine(AnimSpawn());
    }
    private IEnumerator AnimSpawn()
    {
        const int AnimLenght = 30;
        
       
        for ( int i = 1; i <= AnimLenght; i++)
        {
          int distance = (int)(-1.0418 + 0.2544667 * i + 0.9960787 *Math.Pow( i , 2) -0.02232175 * Math.Pow( i, 3));
           int maxwidth = (distance * (rings.Count -1));

            for (int j = 0; j < rings.Count; j++)
            {
                int pos_x = 0;
                
                pos_x -= maxwidth/ 2;
                pos_x += distance * j;
                rings[j].transform.localPosition = new Vector3(pos_x, offset_Y, 0f);

            }
            yield return new WaitForEndOfFrame();

        }
    }

    private IEnumerator AnimDestroy()
    {
        const int AnimLenght = 30;


        for (int i = 1; i <= AnimLenght; i++)
        {
            // = 300.2956 + 0.7586207*x - 1.078161*x^2 + 0.02397373*x^3
            int distance = (int)(-300.2956 + 0.7586207 * i - 1.078161 * Math.Pow(i, 2) + 0.02397373 * Math.Pow(i, 3));
            int maxwidth = (distance * (rings.Count - 1));

            for (int j = 0; j < rings.Count; j++)
            {
                int pos_x = 0;

                pos_x -= maxwidth / 2;
                pos_x += distance * j;
                rings[j].transform.localPosition = new Vector3(pos_x, offset_Y, 0f);

            }
            yield return new WaitForEndOfFrame();

        }
    }

    public override void StartupAnimation()
    {
        foreach (Ring ring in rings)
        {
            ring.StartupAnimation();
        }
        StartCoroutine(AnimSpawn());
    }

    public override void Remove()
    {
        foreach (Ring ring in rings)
        {
            ring.Remove();
        }
        StartCoroutine(AnimDestroy());
    }

    void Update() 
    {
      if (allTriggered()) {
        Interaction from = RadialMenuSpawner.ins.spawningState;
        Interaction to = GetComponent<RingLayout>().goToState;

        from.activeState = false;
        to.activeState = true;
      }
    }

    private bool allTriggered() 
    {
      return count == triggert;
    }
}
