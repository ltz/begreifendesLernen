﻿using UnityEngine;
    public abstract class A_GUILayer : MonoBehaviour
    {
    public bool hidden;
    public abstract void Setup(Action[] action);
    public abstract void StartupAnimation();
    public abstract void Remove();
    }

