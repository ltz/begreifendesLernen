﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;


public class EventManagerOneArg : MonoBehaviour
{
    [System.Serializable]
    class IntEvent : UnityEvent<GameObject>
    {

    }
    private Dictionary<string, IntEvent> eventDictionary;

    private static EventManagerOneArg eventManager;

    public static EventManagerOneArg instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManagerOneArg)) as EventManagerOneArg;

                if (!eventManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, IntEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction<GameObject> listener)
    {
        IntEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new IntEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<GameObject> listener)
    {
        if (eventManager == null) return;
        IntEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, GameObject g)
    {
        IntEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(g);
        }
    }
}
