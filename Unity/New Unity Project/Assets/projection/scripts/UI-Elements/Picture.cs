﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Picture : A_GuiElement
{
    public override void Setup(Action action)
    {
        transform.localPosition = action.positon;

       GetComponentInParent<RectTransform>().sizeDelta = action.size;
        GetComponent<Image>().sprite = action.sprite;

    }

    public override void StartupAnimation()
    {
        throw new System.NotImplementedException();
    }
}
