﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using APPConfig;

public class ButtonIsTriggered : MonoBehaviour
{


    public Image triggerTimer;
    private int triggerTime;

    void OnEnable()
    {
        switch( GetComponent<RadialButton>().buttontype)
        {
            case (ButtonType.Start):
                EventManagerZeroArg.StartListening("DebugButtonID", DebugButtonFound);
                break;
        }
        
    }

    private void OnDisable()
    {
        EventManagerZeroArg.StopListening("DebugButtonID", DebugButtonFound);
    }

    // Debug 
    void DebugButtonFound()
    {
        
        GetComponent<RadialButton>().buttonID = 6;
        EventManagerZeroArg.TriggerEvent("Found");
    }

    private void OnTriggerEnter(Collider other)
    {

        int nowtime = Time.frameCount - this.gameObject.GetComponent<RadialButton>().spawntime;
        triggerTime = Time.frameCount;
        switch (GetComponent<RadialButton>().buttontype)
        {
            case (ButtonType.Start):
                break;

        }
       
        if (nowtime < 5)
        {
            gameObject.GetComponent<RadialButton>().spawnTriggered = true;
        }
    }

    private void OnTriggerStay()
    {
        const float AnimLength = 30.0f;
        if (!gameObject.GetComponent<RadialButton>().spawnTriggered)
        {
            var nowTime = Time.frameCount;
            triggerTimer.fillAmount = 1.0f * (nowTime - triggerTime) / AnimLength;
            EventManagerZeroArg.TriggerEvent("ShowFullText");

            if (1.0f == triggerTimer.fillAmount)
            {
                // Send a event 
                EventManagerZeroArg.TriggerEvent("HideFullText");

                switch (GetComponent<RadialButton>().buttontype)
                {
                    // TODO get column by color of Physical
                    case (ButtonType.Start):

                        EventManagerZeroArg.StartListening("FetchDataComplete", GoNextState);
                        EventManagerOneArg.TriggerEvent("Start", gameObject);
                        EventManagerOneArg.TriggerEvent("FetchData", gameObject);

                        break;
                    case (ButtonType.ChooseElement):
                        EventManagerZeroArg.StartListening("choosen", GoNextState);
                        EventManagerOneArg.TriggerEvent("ChooseElement", gameObject);
                        break;
                    case (ButtonType.ChooseExperiment):
                        GoNextState();
                        break;
                    case (ButtonType.Back): // normal Backbutton USE FOR STATIC STATES
                        GoNextState();
                        break;
                    case (ButtonType.Back_Remove):
                        EventManagerZeroArg.StartListening("removed", GoNextState);
                        EventManagerZeroArg.TriggerEvent("removeChildData");
                        GoNextState();
                        break;
                }

            }
        }


    }
    void GoNextState()
    {

        Interaction from = RadialMenuSpawner.ins.spawningState;
        Interaction to = GetComponent<RadialButton>().goToState;

        from.activeState = false;
        to.activeState = true;
        EventManagerZeroArg.StopListening("FetchDataComplete", GoNextState);

    }

    private void OnTriggerExit(Collider other)
    {
                EventManagerZeroArg.TriggerEvent("HideFullText");
        triggerTimer.fillAmount = 0;
        gameObject.GetComponent<RadialButton>().spawnTriggered = false;
    }
}
