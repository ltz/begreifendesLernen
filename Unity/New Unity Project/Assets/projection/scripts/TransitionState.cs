﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionState : MonoBehaviour {


    public int GoesToState;
    public int Currentstate;
   
    
    void OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<CubeMove>().lastposition.y == this.transform.position.y)
        {


            //collider.GetComponent<CubeMove>().lastposition.y = collider.transform.position.y;
            GameObject[] movable = GameObject.FindGameObjectsWithTag("movable");

            //  collider.GetComponent<CubeMove>().hasMoved = false;
            // collider.GetComponent<CubeMove>().lastposition = collider.GetComponent<Transform>().position;
            foreach (GameObject g in movable)
            {
                Transform t = g.GetComponent<Transform>();
                if (GoesToState < Currentstate)
                {
                    t.position = new Vector3(t.position.x, t.position.y - 10f, t.position.z);
                }
                if (GoesToState > Currentstate)
                {
                    t.position = new Vector3(t.position.x, t.position.y + 10f, t.position.z);
                }
            }
        }

        Debug.Log(message: "transition to state " + GoesToState);
        
    }
    private void OnTriggerExit(Collider other)
    {
        // don't allow trigger after screen transition
        if (other.GetComponent<CubeMove>().lastposition.y != this.transform.position.y)
        {
            other.GetComponent<CubeMove>().lastposition.y = this.transform.position.y;
        }
    }
}