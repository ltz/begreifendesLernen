﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingIsActivated : MonoBehaviour {
    
    public Image activeImage;
    public Image ringImage;
    private RingLayout ringLay;
    public int regColor;
    private int triggerTime;
    // Use this for initialization
    private void Start()
    {
        ringLay = GetComponentInParent<RingLayout>();
    }

    private void OnTriggerEnter(Collider other) 
    {
      triggerTime = Time.frameCount;
      if (ringLay){ ringLay.triggert  += 1;}
    }

    private void OnTriggerStay(Collider other)
    {
      //TODO get  Tracker and Color of physical Object
      //var colorOfPhysical = other.GetComponent<++>().color;
        // Debug 
        var colorOfPhysical = regColor;
        // Debug
       if (colorOfPhysical == regColor && 1.0000001f > activeImage.fillAmount)
        {
        const float AnimLength = 30.0f;
            var nowTime = Time.frameCount;
            activeImage.fillAmount = 1.0f * (nowTime - triggerTime) / AnimLength;
            ringImage.fillAmount = 1.0f - 1.0f * (nowTime - triggerTime) / AnimLength;

        }
    }

    private void OnTriggerExit(Collider other)
    {
      activeImage.fillAmount = 0;
      ringImage.fillAmount = 1;
      if (ringLay){ ringLay.triggert  -= 1;}
       

    }
}
