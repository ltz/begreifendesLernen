﻿using System.Collections;
using System.Collections.Generic;
using CamIO;
using UnityEngine;

public class Cylinder : MonoBehaviour
{
    public TrackedObject obj;
    public Vector3 lastposition;
    public Vector3 position;
    public bool newInfo;

    // Use this for initialization
    void Start()
    {

        obj.PositionChanged += Obj_PositionChanged;
        Debug.Log("start tracked object");
    }

    private void Obj_PositionChanged(object sender, PositionChangedEventArgs e)
    {
        position = new Vector3(-(e.getBoundingBox().X * (1920 / 800f) - 1920 / 2f), 0, (e.getBoundingBox().Y * (1050 / 450f) - 1050 / 2f));
        newInfo = true;
        Debug.Log("object moved");
    }



    // Update is called once per frame
    void Update()
    {
        if (newInfo)
        {

            position.y = transform.localPosition.y;
            transform.localPosition = position;
            newInfo = false;
        }
    }
}
