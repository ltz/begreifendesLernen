﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Interaction : MonoBehaviour
{
    public bool activeState;
    public bool hasInstance = false;

    //public bool finishedDraw = false;
    public List<GameObject> spawned;

    // ButtonEvent buttonActive;

    public List<Action> options;
    // Use this for initialization



    public void nextState(GameObject g)
    {
        // g is State to spawn
        Interaction spawnedFrom = this;
        Interaction spawnNew = g.GetComponent<Interaction>();


        spawnedFrom.activeState = false;
        spawnNew.activeState = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (!activeState && hasInstance)
        {
          // remove screen
            EventManagerOneArg.StopListening("FetchData", fetchData);
            EventManagerOneArg.StopListening("nextState", nextState);
            EventManagerZeroArg.StopListening("removeChildData",  removeChildData);
            RadialMenuSpawner.ins.DeleteMenu(spawned);
            hasInstance = !hasInstance;
        }
        if (activeState && !hasInstance)
        {

            // tell the canvas to spawn a menu
            EventManagerOneArg.StartListening("FetchData", fetchData);
            EventManagerOneArg.StartListening("nextState", nextState);
            EventManagerZeroArg.StartListening("removeChildData",  removeChildData);
            RadialMenuSpawner.ins.SpawnMenu(this);
            hasInstance = !hasInstance;

        }


    }


    void removeChildData() {
      EventManagerZeroArg.StopListening("removeChildData", removeChildData);
      var count = options.Count;
      for (var i = 0 ; i < count; i++)
      {
        var action = options[i];
        if (action.prefab.tag == "A_GUILayer") 
        {
          // remove the childs
          options.RemoveRange(i+1, action.childs);
          // set childcount to -1 // indicates empty parent
          action.childs = -1;
          // and update the count of options
          count = options.Count;
        }
      }
      EventManagerZeroArg.TriggerEvent("removed");
    }



    void fetchData(GameObject game)
    {
      EventManagerOneArg.StopListening("fetchData", fetchData);
        int count = game.GetComponent<RadialButton>().goToState.options.Count;

        for (var i = 0; i < count; i++)
        { // look for empty A_GuiLayer
            var action = game.GetComponent<RadialButton>().goToState.options[i];

            if (action.prefab.tag == "A_GUILayer" )
            { if ( action.childs < 0)
            {// setup the Childs
                PeriodTable pt = GameObject.FindGameObjectWithTag("PeriodTable").GetComponent<PeriodTable>();

                action.childs = pt.GetElemntsInColumn(pt.highlightedColumn);
                string[] texts = pt.GetColumnTexts(pt.highlightedColumn);
                var j = i + 1;
                foreach (var text in texts)
                {
                    Action a = new Action();
                    a.prefab = DATAContainer.ins.button;
                    a.buttontype = APPConfig.ButtonType.ChooseElement;
                    a.text = text;
                    if ("H" == text || "O" == text) {
                    a.goToState = GameObject.Find("AuswahlExperiment").GetComponent<Interaction>();
                    }
                    else {
                    a.goToState = GameObject.Find("AuswahlExperimentEmpty").GetComponent<Interaction>();
                    }

                    game.GetComponent<RadialButton>().goToState.options.Insert(j, a);
                    j++;
                }
            }

        }
        }

        EventManagerZeroArg.TriggerEvent("FetchDataComplete");
    }
}
