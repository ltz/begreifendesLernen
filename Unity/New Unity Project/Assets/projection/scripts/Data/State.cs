﻿using UnityEngine;
using System.Collections;
using System;

public class State : MonoBehaviour
{
    public bool IsOneTime;
    private bool Done;
    public GameObject Function;
    public bool[] toDraw;
    public void Func()
    {
        if (!Done && null != Function)
        {
            //Function.GetComponent<AbstractFunction>().DoStuff();
            if (IsOneTime)
            {
                Done = true;
            }
        }
    }

}
