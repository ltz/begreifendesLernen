﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public   class DATAContainer : MonoBehaviour {

  public static DATAContainer ins;

  public  GameObject button;
  public  GameObject picture;
  public  GameObject ring;
  public  GameObject text;
  public  GameObject video;
  
  void Start() 
  {
    ins = FindObjectOfType(typeof(DATAContainer)) as DATAContainer;
  }

    // Colors
    public static  Color darkblue = new Color(45.0f / 255, 53f / 255, 71f / 255, 1);
    public static  Color orange = new Color(221f / 255, 95f / 255, 38f / 255, 1);
    public static  Color green = new Color(128f / 255, 182f / 255, 146f / 255, 1);
    public static  Color pink = new Color(175f / 255, 56f / 255, 93f / 255, 1);
    public static  Color yellow = new Color(244f / 255, 190f / 255, 36f / 255, 1);
    public static  Color red = new Color(140f / 255, 23f / 255, 43f / 255, 1);
    public static  Color cyan = new Color(84f / 255, 170f / 255, 198f / 255, 1);
   // public static  Color white = new Color(57f / 255, 55f / 255, 55f / 255, 1);
    public static  Color antrazit = new Color(100f / 255, 100f / 255, 100f / 255, 1);
    public static  Color gray = new Color(30f / 255, 30f / 255, 30f / 255, 1);
    public static Color white = gray;
    
}
