﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using APPConfig;

[System.Serializable]
public class Action
{
    // contains all information to draw the right UI element

    public GameObject prefab;// what
    public int childs; // num of childern elements, must occure sequencial after this

    public Vector3 positon; // where 
    public Vector2 size; // size of Rectangle
    public Color color; // background
    public Sprite sprite; // icon overlay
    public string path; // path to resource folder vor video
    public string text;  // text or folder
    public bool hidden;  // should the element be shown
    public bool isPermanent; // is the Ring premanent
    public ButtonType buttontype;
    public int buttonID;
    public string fullText; // long name of element

    public Interaction goToState;
}

