﻿using UnityEngine;
namespace APPConfig
{

    public enum ButtonType {None = -1, Start = 0, ChooseElement = 1, ChooseExperiment = 2, Back = 3, Inactive = 4, Back_Remove = 5}
  
    public enum ButtonSize { SpawnSize = 0, Fullsize = 182} 

    public enum MenuSize { small = 300, large = 350 }
  
}
