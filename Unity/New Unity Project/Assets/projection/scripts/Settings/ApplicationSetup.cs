﻿using UnityEngine;

using System.Collections;

public class ApplicationSetup : MonoBehaviour
{
    public int frameRate = 45;

    void Start()
    {
        
        StartCoroutine(changeFramerate());
    }
    IEnumerator changeFramerate()
    {
        yield return new WaitForSeconds(1);
        Application.targetFrameRate = frameRate;
      
    }
}
