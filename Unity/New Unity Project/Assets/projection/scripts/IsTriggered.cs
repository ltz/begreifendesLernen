﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTriggered : MonoBehaviour {
    
    private void OnTriggerEnter(Collider other)
    {
        Interaction spawnedFrom = RadialMenuSpawner.ins.spawningState;
        Interaction spawnNew = (this.GetComponent<RadialButton>() as RadialButton).goToState;
       
            
        spawnedFrom.activeState = false;
        spawnNew.activeState = true;
     
        
        
    }
}
