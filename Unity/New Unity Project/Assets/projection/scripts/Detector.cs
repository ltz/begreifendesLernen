﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CamIO;
using System.Drawing;

public class Detector : MonoBehaviour {

    public ObjectDetector detector;
    public bool newFrame;
    private bool init;
    public List<Rectangle> objects;
    public string color;

    // Use this for initialization
    void Start () {
        detector = new ObjectDetector();
        if (!detector.connect())
        {
            Debug.Log("Could not find any connected camera device");

        }
        else
        {
            detector.FrameUpdate += Detector_FrameUpdate;
            newFrame = false;
            init = false;
        }
    }

    private void Detector_FrameUpdate(object sender, FrameUpdateEventArgs e)
    {
        objects = e.objects;
        if (e.color == "blue")
        {
        newFrame = true;
            Debug.Log(e.objects.ToString());
        }
    }

    // Update is called once per frame
    void Update () {
        if (newFrame && !init )
        {
            
                EventManagerZeroArg.TriggerEvent("DebugButtonID");

                EventManagerZeroArg.TriggerEvent("nextInternal");
            newFrame = false;
            init = true;
        }

	}

    void OnApplicationQuit()
    {
        detector.stop();
    }

}
