using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenuSpawner : MonoBehaviour
{

    public static RadialMenuSpawner ins;

    public Interaction spawningState;

    public RadialMenu preRadialMenu;
    public RadialButton preRadialButton;
    public Picture prePicture;
    public Text preText;
    public Ring preRing;
    public Video preVideo;
//    public Cylinder preCylinder;
    

    private void Start()
    {
        
    }
    void Awake()
    {
        ins = this;
    }
    // spawns for every option in obj the corresponding UIelement
    public void SpawnMenu(Interaction obj)
    {
        spawningState = obj;
        switch (spawningState.gameObject.name) {
        case ( "Loading" ):
          EventManagerZeroArg.TriggerEvent("ResetColumnColor");
          break;
        case ("AuswahlElement"):
          EventManagerZeroArg.TriggerEvent("ResetElementColor");
          break;
        case ("ExperimentSetup"):
          EventManagerZeroArg.TriggerEvent("ShowRemainElements");
          break;
        case ("AuswahlExperiment"):
          EventManagerZeroArg.TriggerEvent("RevertRemainElements");
          break;
        }
        spawningState.spawned = new List<GameObject>();

        for (int i = 0; i < obj.options.Count; i++)
        {

            switch (obj.options[i].prefab.tag)
            {
                case ("A_GUILayer"):
                    Action[] options = new Action[obj.options[i].childs];

                    obj.options.CopyTo(i + 1, options, 0, obj.options[i].childs);


                    var layer = Instantiate(obj.options[i].prefab);
                    layer.GetComponent<A_GUILayer>().hidden = obj.options[i].hidden;
                   
                    layer.transform.SetParent(transform, false);
                    layer.transform.localPosition = obj.options[i].positon; // right and below of periodTable

                    i += obj.options[i].childs; // jump over children because Radialmenu will take care of them
                    layer.GetComponent<A_GUILayer>().Setup(options);
                    spawningState.spawned.Add(layer);
                    break;
                case ("A_GUIElement"):

                    var gElemnt = Instantiate(obj.options[i].prefab);
                gElemnt.transform.SetParent(transform, false);

                gElemnt.GetComponent<A_GuiElement>().Setup(obj.options[i]);

                spawningState.spawned.Add(gElemnt);
                    break;
            }
           
           
            
        }


    }
    public void DeleteMenu(List<GameObject> sp)
    {
        foreach (GameObject o in sp)
        {

            switch (o.gameObject.tag)
            {
                case "GUIElement":
                    o.GetComponent<A_GuiElement>().Remove();
                    break;
                case "GUILayer":
                    o.GetComponent<A_GUILayer>().Remove();
                    break;
                default:
                    Destroy(o); // without Animation
                    break; 
            }
        }
        sp.Clear();
    }


}
