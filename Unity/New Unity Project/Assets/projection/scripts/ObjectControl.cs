﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CamIO;
using System;

public class ObjectControl : MonoBehaviour
{

    public static ObjectControl ins;
    public CameraManager camControl;
    private TrackedObject obj;
    public GameObject preCylinder;
    public Dictionary<int, Cylinder> objects;
    public bool newObject;
    public int frameCount;

    public void Awake()
    {
        ins = this;
    }
    public void Start()
    {
        camControl = new CameraManager();
        if (!camControl.connect())
        {
            Debug.Log("Could not find any connected camera device");

        }
        else
        {
            camControl.NewObject += CamControl_NewObject;
            camControl.FrameChanged += CamControl_FrameChanged;
            EventManagerZeroArg.StartListening("ObjectFound", OnObjectFound);
            newObject = false;
            frameCount = 0;
        }
        Debug.Log("start");
    }

    private void CamControl_FrameChanged(object sender, FrameChangedEventArgs e)
    {
       // Debug.Log("frame changed");
    }

    void OnApplicationQuit()
    {
        camControl.stop();
    }

    private void OnObjectFound()
    {
        newObject = true;
        //GameObject newCylinder = Instantiate(preCylinder);
        //newCylinder.transform.SetParent(transform, false);
        //newCylinder.GetComponent<Cylinder>().obj = this.obj;
        
        //objects.Add(this.obj.getObjectId(), newCylinder);
    }

    private void CamControl_NewObject(object sender, NewObjectEventArgs e)
    {
        obj = e.obj;
        //EventManagerZeroArg.TriggerEvent("ObjectFound");
        newObject = true;
        Debug.Log("event");
    }
    public void Update()
    {
        if (newObject)
        {
            GameObject newCylinder = Instantiate(preCylinder);
            newCylinder.transform.SetParent(transform, false);
            newCylinder.GetComponent<Cylinder>().obj = this.obj;
            newObject = false;
        }
        if (frameCount<120)
        {
            frameCount++;
        }
        else
        {
            camControl.detect();
            frameCount = 0;
        }

    }
}
