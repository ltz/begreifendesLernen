﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using AIO;
public class BTHandler : MonoBehaviour
{

    private CommunicationManager comMgr;
    private bool canSendCommandsNow = false;
    private bool finished = false;
    private int id;
    // Use this for initialization
    void Start()
    {
        comMgr = new CommunicationManager();
        comMgr.CommandReceived += comMgr_CommandReceived;
        comMgr.ConnectToAll();
        Debug.Log("BT Handler executed");
    }

    void comMgr_CommandReceived(object sender, ReceivedCommandEventArgs e)
    {
        if (!canSendCommandsNow)
        {
            canSendCommandsNow = true;
        }
        id = e.Command.ArduinoID;
        Debug.Log("Command received " + e.Command.ArduinoID);
    }

    // Update is called once per frame
    void Update()
    {
        if (canSendCommandsNow && !finished)
        {
            comMgr.SendCommand(id, CommandType.kLED1Control, new object[] { 0, 1 });
            finished = true;
            Debug.Log("LED command sent");
        }

    }

    IEnumerator connectionUpdates()
    {

        yield return new WaitForSeconds(1);
    }
}