﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using AIO;

namespace BluetoothTest
{
    public partial class Form1 : Form
    {
        CommunicationManager comMgr = new CommunicationManager();
        public Form1()
        {
            InitializeComponent();
            comMgr.CommandReceived += cm_ReceivedCmd;
        }

        void communcationThread()
        {
            /*
            Console.WriteLine("new Thread started, waiting for clients");
            BinaryFormatter bf = new BinaryFormatter();
            localHost = IPAddress.Parse("127.0.0.1");
            listeners = new TcpListener(localHost, 8888);
            listeners.Start();
            while (true)
            {
                clientSocket = listeners.AcceptTcpClient();
                Thread.Sleep(500);
                Console.WriteLine(" >> client recognized");
                try
                {
                    NetworkStream ns = clientSocket.GetStream();
                    while (true)
                    {
                        byte[] bytesFrom = new byte[1024];
                        ns.Read(bytesFrom, 0, clientSocket.Available);
                        Console.WriteLine(" >> Read data from client stream, redirecting to bluetooth");
                        CommunicationObject obj;
                        using (var memStream = new MemoryStream())
                        {
                            memStream.Write(bytesFrom, 0, bytesFrom.Length);
                            memStream.Seek(0, SeekOrigin.Begin);
                            obj = (CommunicationObject)bf.Deserialize(memStream);
                        }

                        if (arduino != null)
                        {
                            ReceivedCommand response;
                            if (obj.arguments == null)
                            {
                                // no arguments given
                                response = arduino.SendCommandBT(obj.ct);
                            }
                            else
                            {
                                response = arduino.SendCommandBT(obj.ct, obj.arguments);
                            }
                            Console.WriteLine(" >> sending to arduino board");
                            CommunicationObject responseObject = new CommunicationObject();
                            responseObject.ct = (AIO.CommandType)response.CmdId;
                            responseObject.arguments = new object[]{response.ReadInt32Arg()};

                            using (var ms = new MemoryStream())
                            {
                                bf.Serialize(ms, responseObject);
                                byte[] sendBytes =  ms.ToArray();
                                ns.Write(sendBytes, 0, sendBytes.Length);
                                ns.Flush();
                                Thread.Sleep(500);
                                Console.WriteLine(" >> Server responded, waiting for new input");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    clientSocket.GetStream().Close();
                    clientSocket.Close();
                    listeners.Stop();
                    Console.WriteLine(" >> exit");
                }
            }*/
        }

        void cm_ConnectionFound(object sender, EventArgs e)
        {
            //arduino = (ArduinoIO) sender;
            //ReceivedCommand answer = arduino.SendCommandResponseBT(AIO.CommandType.kAssign, AIO.CommandType.kOK, 0);
            //if (answer.Ok) // check if response is valid
            //{
            //     this.Invoke((MethodInvoker)(() => listBox1.Items.Add("BOARD IDENTIFIED " + answer.ReadBinInt16Arg() + "!")));
            //     comThread.Start();
            //}
        }

        

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            //button2.Enabled = false;
            //button2.Text = "discovering...";
            //clientComp.DiscoverDevicesAsync(8, true, true, true, true, currentClient);
            comMgr.ConnectToAll();
        }

        void cm_ReceivedCmd(object sender, ReceivedCommandEventArgs e)
        {
            this.Invoke((MethodInvoker)(() => listBox1.Items.Add("Identified boardID = " + e.Command.ArduinoID.ToString())));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            comMgr.SendCommand(50, AIO.CommandType.kLED1Control, new object[] { 0, int.Parse( textBox1.Text) });
        }
    }
}
