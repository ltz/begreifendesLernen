﻿using System;
using System.Collections.Generic;
using CommandMessenger;
using CommandMessenger.Transport;
using CommandMessenger.Transport.Bluetooth;
using AIO;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Linq;

namespace BTServer
{
    class Program
    {
        ArduinoManager arduinoMgr = new ArduinoManager();
        IPAddress localHost;
        TcpListener listeners;
        TcpClient clientSocket;
        BinaryFormatter bf = new BinaryFormatter();
        NetworkStream ns;

        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.ServerRun();
        }

        void ServerRun()
        {
            arduinoMgr.AddBTInformation("98:D3:32:71:17:18", "1234"); //Oxygen

            arduinoMgr.AddBTInformation("98:D3:37:91:08:71", "1234");
            arduinoMgr.AddBTInformation("98:D3:32:71:17:27", "1234");
            arduinoMgr.BTDeviceConnected += arduinoMgr_BTDeviceConnected;

            localHost = IPAddress.Parse("127.0.0.1");
            listeners = new TcpListener(localHost, 8888);
            listeners.Start();
            Console.WriteLine(" >> server listens...");
            while (true)
            {
                try
                {
                    clientSocket = listeners.AcceptTcpClient();
                    Thread.Sleep(500);
                    Console.WriteLine(" >> client recognized");
                    ns = clientSocket.GetStream();
                    while (true)
                    {
                        if (ns.DataAvailable)
                        {
                            byte[] bytesFrom = new byte[1024];
                            ns.Read(bytesFrom, 0, clientSocket.Available);

                            Console.WriteLine(" << read data from client stream, redirecting to bluetooth");
                            CommunicationObject obj;
                            if (bytesFrom.Any(item => item > 0))
                            {
                                using (var memStream = new MemoryStream())
                                {
                                    memStream.Write(bytesFrom, 0, bytesFrom.Length);
                                    memStream.Seek(0, SeekOrigin.Begin);
                                    obj = (CommunicationObject)bf.Deserialize(memStream);
                                }


                                CommunicationObject responseObject = new CommunicationObject();
                                if (obj.ServerCommand == TCPServerCommand.ArduinoCommand)
                                {
                                    ReceivedCommand response; // potential response from BT device
                                    Console.WriteLine(" >> identifying arduino board");
                                    ArduinoIO arduino = arduinoMgr.GetDeviceById(obj.ArduinoID);
                                    if (arduino == null)
                                    {
                                        Console.WriteLine(" >> not a valid arduino board id");
                                        responseObject.ServerCommand = TCPServerCommand.AndroidInformation;
                                        responseObject.Arguments = new object[] { "not a valid ID."};
                                        responseObject.ArduinoID = obj.ArduinoID;
                                    }
                                    else
                                    {
                                        Console.WriteLine(" >> sending to arduino board");
                                        if (obj.Arguments == null)
                                        {
                                            // no arguments given
                                            response = arduino.SendCommandBT(obj.BTCommandType);
                                        }
                                        else
                                        {
                                            response = arduino.SendCommandBT(obj.BTCommandType, obj.Arguments);
                                        }

                                        Console.WriteLine(" << received BT response");
                                        responseObject.BTCommandType = (AIO.CommandType)response.CmdId;
                                        responseObject.Arguments = new object[] { response.ReadInt32Arg() };
                                        responseObject.ArduinoID = obj.ArduinoID;
                                    }
                                }
                                else if (obj.ServerCommand == TCPServerCommand.ArduinoConnect)
                                {
                                    arduinoMgr.ConnectToAll();
                                    responseObject.ServerCommand = TCPServerCommand.ArduinoConnect;
                                }
                                SendViaSocket(responseObject);
                            }
                            else
                            {
                                Console.WriteLine(" >> empty bytes");
                            }
                        } 
                    }
                }
                catch (Exception e)
                {
                    // socket interrupted
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (clientSocket.GetStream() != null)
                        clientSocket.GetStream().Close();
                    clientSocket.Close();
                    listeners.Stop();
                    Console.WriteLine(" >> exit");
                }
            }
        }

        void SendViaSocket(CommunicationObject responseObject)
        {
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, responseObject);
                byte[] sendBytes = ms.ToArray();
                ns.Write(sendBytes, 0, sendBytes.Length);
                ns.Flush();
                Thread.Sleep(500);
                Console.WriteLine(" >> server responded, waiting for new input...");
            }       
        }

        void arduinoMgr_BTDeviceConnected(object sender, InformationEventArgs e)
        {
            SendViaSocket(new CommunicationObject() { 
                    ServerCommand = TCPServerCommand.AndroidInformation,
                    ArduinoID = int.Parse(e.Info)
            });
        }
    }
}
