﻿using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CamIO
{
    public class ObjectDetector
    {
        // Maxmum number of trackable objects
        internal const int MAX_NUM_OF_OBJECTS = 10;

        // Blob area definition
        internal const int MIN_OBJECT_AREA = 100;
        internal const int MAX_OBJECT_AREA = 100 * 70;

        // Output screen dimensions
        internal const int SCREEN_WIDTH = 1920;
        internal const int SCREEN_HEIGHT = 1050;

        // Camera input dimensions
        internal const int CAP_WIDTH = 640;
        internal const int CAP_HEIGHT = 480;

        private VideoCapture capture;

        public event EventHandler<FrameUpdateEventArgs> FrameUpdate;

        private Dictionary<string, Hsv> listValuesHSV = new Dictionary<string, Hsv>();
        private List<string[]> colorList = new List<string[]>();
        public Hsv thresholdColorHigh, thresholdColorLow;
        private CvBlobDetector detector;
        
        private Image<Gray, byte> threshold_image;
        private Mat frame;

        public ObjectDetector()
        {
            colorList.Add(new string[] { "blue_low", "blue_high" });
            listValuesHSV.Add("blue_low", new Hsv(68, 11, 0));
            listValuesHSV.Add("blue_high", new Hsv(145, 255, 122));

            //colorList.Add(new string[] { "yellow_low", "yellow_high" });
            //listValuesHSV.Add("yellow_low", new Hsv(23, 59, 100));
            //listValuesHSV.Add("yellow_high", new Hsv(91, 255, 254));

            //colorList.Add(new string[] { "pink_low", "pink_high" });
            //listValuesHSV.Add("pink_low", new Hsv(164, 100, 100));
            //listValuesHSV.Add("pink_high", new Hsv(195, 255, 254));

            //colorList.Add(new string[] { "orange_low", "orange_high" });
            //listValuesHSV.Add("orange_low", new Hsv(0, 103, 117));
            //listValuesHSV.Add("orange_high", new Hsv(15, 255, 255));

            //colorList.Add(new string[] { "lightblue_low", "lightblue_high" });
            //listValuesHSV.Add("lightblue_low", new Hsv(31, 34, 25));
            //listValuesHSV.Add("lightblue_high", new Hsv(80, 210, 197));

            this.detector = new CvBlobDetector();
        }

        public bool connect()
        {

            try
            {
                capture = new VideoCapture();
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, CAP_WIDTH);
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, CAP_HEIGHT);
                if (capture != null)
                {
                    capture.ImageGrabbed += Capture_ImageGrabbed;
                    capture.Start();
                    //timer.Start();
                }
            }
            catch (NullReferenceException excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return true;
        }

        public void stop()
        {
            capture.Stop();
            capture.Dispose();
        }

        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            using (Image<Bgr, byte> image = new Image<Bgr, Byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT)))
            {
                capture.Retrieve(image); // get new frame from webcam
                Image<Hsv, Byte> imageHSV = image.Convert<Hsv, Byte>(); // convert to HSV space



                threshold_image = imageHSV.InRange(thresholdColorLow, thresholdColorHigh); // apply color filter
                threshold_image.SmoothMedian(5);
                threshold_image = threshold_image.Erode(5);
                threshold_image = threshold_image.Dilate(2);

                this.frame = image.Mat;




                List<Rectangle> foundObjects;
                // go through every color registered
                foreach (string[] color in colorList)
                {
                    foundObjects = detectInRange(image, listValuesHSV[color[0]], listValuesHSV[color[1]]); // detect blobs
                    String colorName = color[0].Split((char)95)[0];
                    //foreach (CvBlob blob in blobs.Values)
                    //{

                    //    if (!hasBeenDetected(blob.BoundingBox, colorName))
                    //    {
                    //        addObject(blob.BoundingBox, colorName);
                    //        image.Draw(blob.BoundingBox, new Bgr(System.Drawing.Color.Red));
                    //    }


                    //}
                    if (foundObjects.Count > 0)
                    {
#if (CS_6)
            FrameUpdate?.Invoke(this, new FrameChangedEventArgs(threshold_image.Bitmap, image.Bitmap));
#else
                        if (FrameUpdate != null)
                        {
                            //FrameChanged(this, new FrameChangedEventArgs(threshold_image.Bitmap, image.Bitmap));
                            FrameUpdate(this, new FrameUpdateEventArgs(foundObjects, colorName));
                        }
#endif
                    }
                }

                //frameCount++;

                //}

            }
        }
        private List<Rectangle> detectInRange(Image<Bgr, Byte> image, Hsv thresholdColorLow, Hsv thresholdColorHigh)
        {
            CvBlobs blobs = new CvBlobs();
            List<Rectangle> foundObjects= new List<Rectangle>();
            Image<Hsv, Byte> imageHSV = image.Convert<Hsv, Byte>(); // convert to HSV space

            threshold_image = imageHSV.InRange(thresholdColorLow, thresholdColorHigh); // apply color filter
                                                                                       // morphological operations making edges more clear
            threshold_image.SmoothMedian(5);
            threshold_image = threshold_image.Erode(5);
            threshold_image = threshold_image.Dilate(2);
            // use built-in blob detector
            detector.Detect(threshold_image, blobs);
            // filter out the relevant blobs
            blobs.FilterByArea(MIN_OBJECT_AREA, MAX_OBJECT_AREA);
            //Console.WriteLine(blobs.Count.ToString()); // DEBUG
            foreach (CvBlob blob in blobs.Values)
            {
                foundObjects.Add(blob.BoundingBox);
            }
            return foundObjects;

        }
    }

    public class FrameUpdateEventArgs: EventArgs
    {
        public List<Rectangle> objects;
        public string color;

        public FrameUpdateEventArgs(List<Rectangle> objects, string color)
        {
            this.objects = objects;
            this.color = color;
        }
    }
}
