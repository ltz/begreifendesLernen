﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Emgu.CV.Structure;
using Emgu.Util;
namespace CamIO
{
    public class ConfigReader
    {
        private string fileNameTest = "config.xml";
        private string fileName = "Assets/Resources/config.xml";
        private XmlDataDocument xmlDoc = new XmlDataDocument();
        private XmlReader xReader;

        private Dictionary<string, KeyValuePair<Hsv,Hsv>> colorList = new Dictionary<string, KeyValuePair<Hsv,Hsv>>();
        private Dictionary<string, KeyValuePair<int, int>> blobSizeList = new Dictionary<string, KeyValuePair<int, int>>();

        public void LoadConfig()
        {
            if (File.Exists(fileName))
            {
                xmlDoc.Load(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            }
            else
            {
                xmlDoc.Load(new FileStream(fileNameTest, FileMode.Open, FileAccess.Read));
            }
            XmlNodeList colors = xmlDoc.DocumentElement.SelectNodes("/config/detection_color");
            foreach (XmlNode node in colors){
                string colorName = node.Name;
                Hsv[] boundaryColors = new Hsv[2];
                int i = 0;
                // lower boundary color
                foreach (XmlNode colorNode in node.ChildNodes)
                {
                    Hsv color = new Hsv(double.Parse(colorNode.Attributes["h"].Value), double.Parse(colorNode.Attributes["h"].Value), double.Parse(colorNode.Attributes["v"].Value));
                    boundaryColors[i++] = color;
                }
                colorList.Add(colorName, new KeyValuePair<Hsv, Hsv>(boundaryColors[0], boundaryColors[1]));
            }

            XmlNodeList sizes = xmlDoc.DocumentElement.SelectNodes("/config/detection_blobsize");
            foreach (XmlNode node in sizes)
            {
                string colorName = node.Attributes["ref"].Value;
                int min = int.Parse(node.Attributes["min"].Value);
                int max = int.Parse(node.Attributes["max"].Value);
            }
        }

        public Dictionary<string, KeyValuePair<Hsv, Hsv>> ColorBoundaries
        {
            get
            {
                return colorList;
            }
        }

        public Dictionary<string, KeyValuePair<int, int>> BlobSizes
        {
            get
            {
                return blobSizeList;
            }
        }
    }
}
