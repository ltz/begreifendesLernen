﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Tracking;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CamIO
{
    /// <summary>
    /// Active tracked object within the image space
    /// </summary>
    public class TrackedObject : IDisposable
    {
        internal static int lastId = 0;
        private int id;
        private string color;
        private Rectangle boundingBox;
        private TrackerKCF tracker;
        //private TrackerMIL tracker;

        public event EventHandler<PositionChangedEventArgs> PositionChanged;

        private int lost = 0; // Indicates how many times the object was not found 
        private bool disposed = false;

        public TrackedObject()
        {

        }

        internal TrackedObject(Rectangle rect, string color)
        {
            this.boundingBox = rect;
            this.id = lastId++;
            this.color = color;
            // Create and initialize the tracker
            this.tracker = new TrackerKCF();
            //this.tracker = new TrackerMIL(3, 65, 25, 4, 100000, 65, 250);
        }

        /// <summary>
        /// Gets the assigned id of the object
        /// </summary>
        /// <returns> </returns>
        public int getObjectId() { return id; }
        public string getColor() { return color; }


        /// <summary>
        /// Initializes a tracking object instance with the appropriate frame
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public bool init(Mat frame)
        {
            return tracker.Init(frame, this.boundingBox);

        }

        private bool equals(TrackedObject obj)
        {
            if (this.color.Equals(obj.getColor()))
            {
                if (this.boundingBox.Contains(obj.getBoundingBox()) || obj.getBoundingBox().Contains(this.boundingBox))
                {

                }
                Rectangle threshold = Rectangle.Intersect(this.boundingBox, obj.getBoundingBox());
                return (Area(threshold) > (2 / 3f) * Area(this.boundingBox)) ? true : false;
            }
            return false;
        }

        internal bool equals(Rectangle bBox, string color)
        {
            
            if (this.color.Equals(color))
            {
                if (!(this.boundingBox.Contains(bBox) || bBox.Contains(this.boundingBox)))
                {
                    Rectangle threshold = Rectangle.Intersect(this.getBoundingBox(), bBox);
                  
                    return (Area(threshold) > ((2/3f) * Area(this.boundingBox))) ? true : false;
                }
                else
                {
                    return true;

                }

            }
            else
                return false;
        }

        internal static float Area(Rectangle rect)
        {
            return (float) rect.Width * rect.Height;
        }
        public Rectangle getBoundingBox() { return this.boundingBox; }

        /// <summary>
        /// Returns if the tracked object is moved to given position
        /// </summary>
        /// <param name="rectPosition"> position to be moved to</param>
        /// <returns></returns>
        public bool isMoved(Rectangle rectPosition)
        {
            return rectPosition.Contains(boundingBox);
        }


        /// <summary>
        /// Update the tracker, find the new most likely bounding box for the object
        /// </summary>
        /// <param name="frame"> Current frame, where the object should be found </param>
        /// <returns> false, if the object cannot be found within 30 frames </returns>
        internal bool update(Mat frame)
        {
            Rectangle old_boundingBox = this.boundingBox;

            // If the object cannot be found within 30 frames in a row,
            // initialize the tracker again with the last know boundingBox of the object
            if (!tracker.Update(frame, out this.boundingBox))
            {
                if (this.lost < 60)
                {
                    this.boundingBox = old_boundingBox;
                    this.lost++;

                    return true;
                }
                else
                {
                    this.lost = 0;
                    return tracker.Init(frame, this.boundingBox);
                }
            }

            this.lost = 0;

            // send an event if the object was moved and its position was changed

#if (CS_6)
            PositionChanged?.Invoke(this, new PositionChangedEventArgs( this.boundingBox));
#else
            if (PositionChanged != null)
            {
                PositionChanged(this, new PositionChangedEventArgs(this.boundingBox));
            }
#endif

            return false;
        }

        /// <summary>
        /// Calculates the position of the tracked object
        /// </summary>
        /// <returns> position </returns>
        public PointF position()
        {
            return new PointF(boundingBox.Location.X + boundingBox.Width / 2, boundingBox.Location.Y + boundingBox.Height / 2);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                tracker.Dispose();

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }
    }

    public class PositionChangedEventArgs : EventArgs
    {
        private Rectangle boundingBox { get; set; }

        public PositionChangedEventArgs(Rectangle boundingBox)
        {
            this.boundingBox = boundingBox;
        }

        /// <summary>
        /// Gets the surrounding box of the tracked object in camera frame coordinates
        /// </summary>
        /// <returns></returns>
        public Rectangle getBoundingBox()
        {
            return this.boundingBox;
        }

    }
}
