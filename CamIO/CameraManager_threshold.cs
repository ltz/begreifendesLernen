﻿//#define CS_6
#undef CS_6
using System;
using Emgu.CV;
using Emgu.CV.Util;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using Emgu.CV.BgSegm;
using System.Windows;
namespace CamIO
{
    /// <summary>
    /// Interface for the USB Webcam and image processing
    /// </summary>
    public class CameraManager_threshold
    {
        // Maxmum number of trackable objects
        internal const int MAX_NUM_OF_OBJECTS = 10;

        // Blob area definition
        internal const int MIN_OBJECT_AREA = 100;
        internal const int MAX_OBJECT_AREA = 100 * 70;

        // Output screen dimensions
        internal const int SCREEN_WIDTH = 1920;
        internal const int SCREEN_HEIGHT = 1050;

        // Camera input dimensions
        internal const int CAP_WIDTH = 640;
        internal const int CAP_HEIGHT = 480;

        //internal const int CAP_WIDTH = 640;
        //internal const int CAP_HEIGHT = 480;

        // time span for detection delay after calling detect
        private int detectionDelay = 120; // delay detection for 120 frame grabs
        private int currentFrameDelay = 0;
        private bool initFrameDelay = true;

        // tracked objects
        private Dictionary<int, TrackedObject> objects;

        // image processing variables
        private VideoCapture capture;  //takes images from webcam as image frames
        private Mat frame;
        private Image<Bgr, Byte> image;

        private Image<Gray, Byte> threshold_image;

        /// <summary>
        /// Called when the frame of the webcam changed
        /// </summary>
        public event EventHandler<FrameChangedEventArgs> FrameChanged;

        /// <summary>
        /// Called when an object has been moved
        /// </summary>
        public event EventHandler<PositionChangedEventArgs> ObjectsMoved;
        public event EventHandler<NewObjectEventArgs> NewObject;
        private Dictionary<string, Mat> threshold_images = new Dictionary<string, Mat>();
        private Dictionary<string, int[]> blob_size = new Dictionary<string, int[]>();
        private Dictionary<string, Hsv> listValuesHSV = new Dictionary<string, Hsv>();
        private List<string[]> colorList = new List<string[]>();
        public Hsv thresholdColorHigh, thresholdColorLow;
        private CvBlobDetector detector;
        private BackgroundSubtractorMOG backgroundSubtractor;

        // Detection toggle
        private bool detectorOn;
        private Image<Bgr, byte> background;

        /// <summary>
        /// Initialize a new instance with default colors blue, yellow and pink
        /// </summary>
        public CameraManager_threshold()
        {
            //colorList.Add(new string[] { "blue_low", "blue_high" });
            //listValuesHSV.Add("blue_low", new Hsv(68, 11, 0));
            //listValuesHSV.Add("blue_high", new Hsv(145, 255, 122));

            //colorList.Add(new string[] { "yellow_low", "yellow_high" });
            //listValuesHSV.Add("yellow_low", new Hsv(23, 59, 100));
            //listValuesHSV.Add("yellow_high", new Hsv(91, 255, 254));

            //colorList.Add(new string[] { "pink_low", "pink_high" });
            //listValuesHSV.Add("pink_low", new Hsv(164, 100, 100));
            //listValuesHSV.Add("pink_high", new Hsv(195, 255, 254));

            colorList.Add(new string[] { "orange_low", "orange_high" });
            listValuesHSV.Add("orange_low", new Hsv(0, 103, 117));
            listValuesHSV.Add("orange_high", new Hsv(15, 255, 255));
            
            colorList.Add(new string[] { "lightblue_low", "lightblue_high" });
            listValuesHSV.Add("lightblue_low", new Hsv(25, 77, 0));
            listValuesHSV.Add("lightblue_high", new Hsv(94, 172, 202));
            //listValuesHSV.Add("lightblue_low", new Hsv(80, 98, 100));
            //listValuesHSV.Add("lightblue_high", new Hsv(111, 175, 187));

            blob_size.Add("lightblue", new int[] { 100, 400 });
            blob_size.Add("orange", new int[] { 900, 100*23 });
            threshold_images.Add("lightblue", new Mat());
            threshold_images.Add("orange",new Mat());

            //this.thresholdColorLow = new Hsv(80, 98, 100);
            //this.thresholdColorHigh = new Hsv(111, 175, 187);
            this.objects = new Dictionary<int, TrackedObject>();
            this.detector = new CvBlobDetector();
            this.threshold_image = new Image<Gray, byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT));
           
        }

        /// <summary>
        /// Get a tracked object by its ID
        /// </summary>
        /// <param name="id"> object ID </param>
        /// <returns> a TrackedObject instance </returns>
        public TrackedObject GetObjectById(int id)
        {
            return objects[id];
        }

        /// <summary>
        /// Try to connect with a webcam
        /// </summary>
        public bool connect()
        {

            try
            {
                capture = new VideoCapture();
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, CAP_WIDTH);
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, CAP_HEIGHT);
                if (capture != null)
                {
                    capture.ImageGrabbed += Capture_ImageGrabbed;
                    capture.Start();
                    //timer.Start();
                }
            }
            catch (NullReferenceException excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return true;
        }

        /// <summary>
        /// Set threshold color manually
        /// </summary>
        /// <param name="high">Upper bound HSV value</param>
        /// <param name="low">Lower bound HSV value</param>
        public void setThresholdColor(Hsv high, Hsv low)
        {
            this.thresholdColorHigh = high;
            this.thresholdColorLow = low;
        }

        /// <summary>
        /// Event handler for EmguCV´s ImageGrabbed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Capture_ImageGrabbed(object sender, EventArgs e)
        {
            //using (Image<Gray, byte> threshold_image = new Image<Gray, byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT)))
            //{
            using (Image<Bgr, byte> image = new Image<Bgr, Byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT)))
            {
                capture.Retrieve(image); // get new frame from webcam
                Image<Hsv, Byte> imageHSV = image.Convert<Hsv, Byte>(); // convert to HSV space




                this.threshold_image = imageHSV.InRange(thresholdColorLow, thresholdColorHigh); // apply color filter
                this.threshold_image.SmoothMedian(5);
                this.threshold_image = threshold_image.Erode(5);
                this.threshold_image = threshold_image.Dilate(3);

                this.frame = image.Mat;
                updateThresholdFrame(image);
                //Update the postions of all objects
                foreach (TrackedObject obj in objects.Values)
                {
                    obj.update(threshold_images[obj.getColor()]);

                    image.Draw(obj.getBoundingBox(), new Bgr(System.Drawing.Color.Red));

                }

                if (!detectorOn) //&& initFrameDelay)
                {
                    // wait until detection phase
                    if (currentFrameDelay < detectionDelay)
                    {
                        currentFrameDelay++;
                    }
                    else
                    {
                        currentFrameDelay = 0;
                        //initFrameDelay = false;
                        detect();
                        Console.WriteLine("Start detection!");
                    }
                }
                else if (detectorOn)
                {
                    CvBlobs blobs;

                    // go through every color registered
                    foreach (string[] color in colorList)
                    {
                        String colorName = color[0].Split((char)95)[0];
                        blobs = detectInRange(image, listValuesHSV[color[0]], listValuesHSV[color[1]], colorName); // detect blobs
                        foreach (CvBlob blob in blobs.Values)
                        {

                            if (!hasBeenDetected(blob.BoundingBox, colorName))
                            {
                                if (addObject(blob.BoundingBox, colorName))
                                {
                                    image.Draw(blob.BoundingBox, new Bgr(System.Drawing.Color.Red));
                                }
                            }


                        }

                    }
                    this.detectorOn = false;
                }
                //frameCount++;
#if (CS_6)
            FrameChanged?.Invoke(this, new FrameChangedEventArgs(threshold_image.Bitmap, image.Bitmap));
#else
                if (FrameChanged != null)
                {
                    //FrameChanged(this, new FrameChangedEventArgs(threshold_image.Bitmap, image.Bitmap));
                    FrameChanged(this, new FrameChangedEventArgs(this.threshold_image.Bitmap, image.Bitmap));
                }
#endif
                //}

            }


        }

        private void handlePositionChanged(object sender, PositionChangedEventArgs args)
        {
            PositionChangedEventArgs externalArgs = new PositionChangedEventArgs(args.getBoundingBox());
#if (CS_6)
            ObjectsMoved?.Invoke(sender, externalArgs);
#else
            if (ObjectsMoved != null)
            {
                ObjectsMoved(sender, externalArgs);
            }
#endif
        }

        /// <summary>
        /// Stoping the webcam
        /// </summary>
        public void stop()
        {
            capture.Stop();
            capture.Dispose();
        }

        /// <summary>
        /// Starts detection phase
        /// </summary>
        public void detect()
        {
            this.detectorOn = true;
        }

        private CvBlobs detectInRange(Image<Bgr, Byte> image, Hsv thresholdColorLow, Hsv thresholdColorHigh,string color)
        {
            CvBlobs blobs = new CvBlobs();
            Image<Hsv, Byte> imageHSV = image.Convert<Hsv, Byte>(); // convert to HSV space
            Image<Gray, Byte> threshold_image = new Image<Gray, byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT));
            threshold_image = imageHSV.InRange(thresholdColorLow, thresholdColorHigh); // apply color filter
                                                                                       // morphological operations making edges more clear
            threshold_image.SmoothMedian(3);
            threshold_image = threshold_image.Erode(4);
            threshold_image = threshold_image.Dilate(3);
            // use built-in blob detector
            detector.Detect(threshold_image, blobs);
            // filter out the relevant blobs
            blobs.FilterByArea(blob_size[color][0], blob_size[color][1]);
            //Console.WriteLine(blobs.Count.ToString()); // DEBUG
            return blobs;

        }
        public bool hasBeenDetected(Rectangle boundingBox, string color)
        {

            foreach (TrackedObject obj in this.objects.Values)
            {
                if (obj.equals(boundingBox, color))
                {
                    return true;
                }
            }
            return false;
        }

        private void updateThresholdFrame(Image<Bgr,Byte> image)
        {
            // go through every color registered
            foreach (string[] color in colorList)
            {
                String colorName = color[0].Split((char)95)[0];
                Image<Hsv, Byte> imageHSV = image.Convert<Hsv, Byte>(); // convert to HSV space
                Image<Gray, Byte> threshold_image = new Image<Gray, byte>(new Bitmap(CAP_WIDTH, CAP_HEIGHT));
                threshold_image = imageHSV.InRange(listValuesHSV[color[0]], listValuesHSV[color[1]]); // apply color filter
                                                                                           // morphological operations making edges more clear
                threshold_image.SmoothMedian(3);
                threshold_image = threshold_image.Erode(4);
                threshold_image = threshold_image.Dilate(3);
                threshold_images[colorName] = threshold_image.Convert<Bgr,Byte>().Mat;
            }
            
        }

        public int Contains(Rectangle boundingBox, string color)
        {

            foreach (TrackedObject obj in this.objects.Values)
            {
                if (obj.equals(boundingBox, color))
                {
                    return obj.getObjectId();
                }
            }
            return -1;

        }
        /// <summary>
        /// Adds a new TrackedObject
        /// </summary>
        /// <param name="boundingBox"> Bounding box of the object </param>
        public bool addObject(Rectangle boundingBox, string color)
        {
            //if (objects.Count < MAX_NUM_OF_OBJECTS)
            //{
            TrackedObject newObj = new TrackedObject(boundingBox, color);

            if (!newObj.init(this.frame))
            {
                return false;
            }
            else
            {
                int id = newObj.getObjectId();
                objects.Add(id, newObj);
                newObj.PositionChanged += handlePositionChanged;
                Console.WriteLine(objects.Count.ToString());
#if (CS_6)
            NewObject?.Invoke(this, new NewObjectEventArgs(newObj));
#else
                if (NewObject != null)
                {
                    NewObject(this, new NewObjectEventArgs(newObj));
                }
#endif
                return true;
            }
        }
        //return false;

        //}
    }

    

    
}
