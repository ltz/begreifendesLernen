interface Action {
  void action(Compound c1, Compound c2);
}
