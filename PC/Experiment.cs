using AIO;
using System.Collections.Generic;

namespace BegreifendesLernen
{
    /// <summary>
    /// 
    /// </summary>
    public class Experiment
    {
        // Propreties
        List<Reaction> reactions;
        AIO.ArduinoIO arduino;
        //KinectObj tracked;


        /// <summary>
        /// 
        /// </summary>
        public Experiment()
        {
            reactions = new List<Reaction>();
            /* hardgecoded H2O to 2H + O 
             * and back again
             */

            // TODO connection zu allen Arduinos
            //
            // Erstelle ein Array der beteiligten Elemente
            Element o = new Element("Sauerstoff", "O");
            Element h1 = new Element("Wasserstoff", "H");
            Element h2 = new Element("Wasserstoff", "H");
            Element[] e = { o, h1, h2 };


            // identifiziere alle beteiligten Objekte/Würfel
            for (int i = 0; i < e.Length; i++)
            {
                // setze idArduino 
                e[i].idArduino = i;
                
                // sende erstem Arduino den befehl LED an
                arduino.SendCommand(CommandType.LED_CONTROL, 1, (byte)LEDControls.LED1_BLUE);
                
                // frage kinect welches Object eine andere Farbe hat
                //e[i].idKinect = tracked.getColoredObject();
                
                // sende erstem Arduino den befehl LED aus
                arduino.SendCommand(CommandType.LED_CONTROL, 0, (byte)LEDControls.LED1_BLUE);
            }




            // Verbinde diese zu Wasser
            Element[] e1 = { o, h1 };
            Element[] e2 = { o, h2 };
            Bound b1 = new Bound(e1, "einfach");
            Bound b2 = new Bound(e2, "einfach");
            Bound[] b = { b1, b2 };

            //o.bounds={b1,b2};
            //h1.bounds={b1};
            //h2.bounds={b2};

            Compound c0 = new Compound(e, null); // einzelne Elemente
            Compound c1 = new Compound(e, b);   // Wasser


            // erstelle ActionSets für c0 -> c1 und c1 -> c0
            //
            /* c0 -> c1 // Vorbereitung: Zusammenbau von Wasser
             * Herstellen aller Bounds
             *   wiederhole für alle bounds b von c1
             *   x = b.elements[0]
             *   y = b.elements[1] // vereinfachtes Model y hat keine Arduino
             *   x = LED an
             *   x = MAGNET_1 an
             *   warte auf x.HALLSENSOR = true
             *   prüfe ob x.nextTo(y) = true // mittels kinect
             */
            Action a1 = new CreateBound(c0,c1);

            ///* c1 -> c0 // Zerlegung von Wasser mittels Kat/Anode
            // * Zerstören aller Bounds
            // * wiederhole für alle bounds b von c1
            // * x = b.elements[0]
            // * x : VIBRA an (timed)
            // * x : MAGNET_ALL aus
            // * anzeige die Zerlegten Componeten zu sortieren
            // * überprüfe ob die zerlegten Componeten richtig sortiert wurden (kinect)
            //
            a2 = new DestroyBound(c1,c0);

            // vorbereitung der Proben 
            a3 = new PlaceObjects(c0, null);

            // Knallgasprobe
            a4 = new Knallgas(c0,c1);



            // füge Compounds und Actions zu 'reactions' hinzu
            reactions.Add(new Reaction(c0, c1, a1));
            reactions.Add(new Reaction(c0, c1, a2));
            reactions.Add(new Reaction(c0, c0, a3));
            reactions.Add(new Reaction(c0, c1, a4));

        }
    }
}
