﻿namespace ArduinoTesting
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnArduinoCon = new System.Windows.Forms.Button();
            this.lblConStat = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMagnetStat = new System.Windows.Forms.Label();
            this.btnMagnet = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.numUpDownLED1 = new System.Windows.Forms.NumericUpDown();
            this.button4 = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.btnArduinoState = new System.Windows.Forms.Button();
            this.lblASDesc = new System.Windows.Forms.Label();
            this.txtArduinoState = new System.Windows.Forms.TextBox();
            this.lblLastState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownLED1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnArduinoCon
            // 
            this.btnArduinoCon.Location = new System.Drawing.Point(469, 17);
            this.btnArduinoCon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnArduinoCon.Name = "btnArduinoCon";
            this.btnArduinoCon.Size = new System.Drawing.Size(101, 36);
            this.btnArduinoCon.TabIndex = 0;
            this.btnArduinoCon.Text = "detect arduino";
            this.btnArduinoCon.UseVisualStyleBackColor = true;
            this.btnArduinoCon.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblConStat
            // 
            this.lblConStat.AutoSize = true;
            this.lblConStat.Location = new System.Drawing.Point(315, 27);
            this.lblConStat.Name = "lblConStat";
            this.lblConStat.Size = new System.Drawing.Size(97, 17);
            this.lblConStat.TabIndex = 1;
            this.lblConStat.Text = "Not connected.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Arduino connection status:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Magnet status";
            // 
            // label4
            // 
            this.lblMagnetStat.AutoSize = true;
            this.lblMagnetStat.Location = new System.Drawing.Point(315, 83);
            this.lblMagnetStat.Name = "label4";
            this.lblMagnetStat.Size = new System.Drawing.Size(27, 17);
            this.lblMagnetStat.TabIndex = 4;
            this.lblMagnetStat.Text = "off.";
            // 
            // btnMagnet
            // 
            this.btnMagnet.Location = new System.Drawing.Point(469, 73);
            this.btnMagnet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMagnet.Name = "btnMagnet";
            this.btnMagnet.Size = new System.Drawing.Size(101, 36);
            this.btnMagnet.TabIndex = 5;
            this.btnMagnet.Text = "toggle ";
            this.btnMagnet.UseVisualStyleBackColor = true;
            this.btnMagnet.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "LED blinking";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(469, 134);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 36);
            this.button3.TabIndex = 8;
            this.button3.Text = "transmit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // numUpDownLED1
            // 
            this.numUpDownLED1.Location = new System.Drawing.Point(318, 145);
            this.numUpDownLED1.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numUpDownLED1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numUpDownLED1.Name = "numUpDownLED1";
            this.numUpDownLED1.Size = new System.Drawing.Size(103, 25);
            this.numUpDownLED1.TabIndex = 9;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(469, 193);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 36);
            this.button4.TabIndex = 11;
            this.button4.Text = "transmit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(318, 201);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(103, 25);
            this.numericUpDown2.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Vibrator status";
            // 
            // btnArduinoState
            // 
            this.btnArduinoState.Location = new System.Drawing.Point(469, 260);
            this.btnArduinoState.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnArduinoState.Name = "btnArduinoState";
            this.btnArduinoState.Size = new System.Drawing.Size(101, 36);
            this.btnArduinoState.TabIndex = 14;
            this.btnArduinoState.Text = "transmit";
            this.btnArduinoState.UseVisualStyleBackColor = true;
            this.btnArduinoState.Click += new System.EventHandler(this.btnArduinoState_Click);
            // 
            // lblASDesc
            // 
            this.lblASDesc.AutoSize = true;
            this.lblASDesc.Location = new System.Drawing.Point(39, 270);
            this.lblASDesc.Name = "lblASDesc";
            this.lblASDesc.Size = new System.Drawing.Size(86, 17);
            this.lblASDesc.TabIndex = 15;
            this.lblASDesc.Text = "Arduino state";
            // 
            // txtArduinoState
            // 
            this.txtArduinoState.Location = new System.Drawing.Point(357, 267);
            this.txtArduinoState.MaxLength = 1;
            this.txtArduinoState.Name = "txtArduinoState";
            this.txtArduinoState.Size = new System.Drawing.Size(64, 25);
            this.txtArduinoState.TabIndex = 16;
            this.txtArduinoState.Text = "0";
            this.txtArduinoState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblLastState
            // 
            this.lblLastState.AutoSize = true;
            this.lblLastState.Location = new System.Drawing.Point(265, 270);
            this.lblLastState.Name = "lblLastState";
            this.lblLastState.Size = new System.Drawing.Size(77, 17);
            this.lblLastState.TabIndex = 17;
            this.lblLastState.Text = "Last state: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 341);
            this.Controls.Add(this.lblLastState);
            this.Controls.Add(this.txtArduinoState);
            this.Controls.Add(this.lblASDesc);
            this.Controls.Add(this.btnArduinoState);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.numUpDownLED1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnMagnet);
            this.Controls.Add(this.lblMagnetStat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblConStat);
            this.Controls.Add(this.btnArduinoCon);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "ArduinoTesting";
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownLED1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnArduinoCon;
        private System.Windows.Forms.Label lblConStat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMagnetStat;
        private System.Windows.Forms.Button btnMagnet;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NumericUpDown numUpDownLED1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnArduinoState;
        private System.Windows.Forms.Label lblASDesc;
        private System.Windows.Forms.TextBox txtArduinoState;
        private System.Windows.Forms.Label lblLastState;
    }
}

