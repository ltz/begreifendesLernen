﻿using System;
using AIO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ArduinoTesting
{
    public partial class MainForm : Form
    {

        private ArduinoManager manager = new ArduinoManager();
        private Thread arduinoThread = new Thread(new ThreadStart(arduinoHandler));

        private List<byte> listStates = new List<byte>();
        private int stateId = 0;
        public MainForm()
        {
            InitializeComponent();

            
            // generate states
            //listStates.Add(comArduino.GenerateState(false, false, false, false, false));
            //listStates.Add(comArduino.GenerateState(false, false, false, true, false));
            //listStates.Add(comArduino.GenerateState(false, false, false, true, true));
            //listStates.Add(comArduino.GenerateState(true, false, false, true, true));
            //listStates.Add(comArduino.GenerateState(true, false, false, true, false));
            //listStates.Add(comArduino.GenerateState(true, false, true, false, false));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblConStat.Text = "trying to connect...";
            if (manager.ConnectToAllSerial() > 0)
            {
                lblConStat.Text = "Connected!";
                btnArduinoCon.Enabled = false;
                //arduinoThread.Start();
            }
            else
            {
                lblConStat.Text = "Error!";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //short answer;
            //if (lblMagnetStat.Text == "off.")
            //{
            //    answer = comArduino.SendCommand(AIO.CommandType.MAGNET_CONTROL, 1);
            //}
            //else
            //{
            //    answer = comArduino.SendCommand(AIO.CommandType.MAGNET_CONTROL, 0);
            //}
            //if (answer == 0)
            //{
            //    lblMagnetStat.Text = "off.";
            //}
            //else if(answer == 1)
            //{
            //    lblMagnetStat.Text = "on.";
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //if (numUpDownLED1.Value > 0)
            //{
            //    comArduino.SendCommand(AIO.CommandType.LED_CONTROL,(short) numUpDownLED1.Value);
            //}
            //else
            //{
            //    comArduino.SendCommand(AIO.CommandType.LED_CONTROL, -1);
            //}
        }

        public static void arduinoHandler()
        {
            //while (true)
            //{
            //    short value = comArduino.WaitReplySerial();
            //    if (value == 1)
            //    {
            //        Console.WriteLine("Field change detected");
            //    }
            //    else
            //    {
            //        Console.WriteLine("off");
            //    }
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //short answer =  comArduino.SendCommand(AIO.CommandType.VIBRATION_CONTROL, 3, (short) numericUpDown2.Value);
        }

        private void btnArduinoState_Click(object sender, EventArgs e)
        {
            //int newstat = int.Parse(txtArduinoState.Text);
            //if (newstat >= 0 && newstat < listStates.Count)
            //{
            //    stateId = newstat;
            //    short answer = comArduino.SendCommand(AIO.CommandType.STATE_TRANSITION, listStates[newstat], (short) stateId);
            //    lblLastState.Text = "Last state: " + answer;
            //}
        }
    }
}
