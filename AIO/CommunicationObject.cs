﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIO
{
    /// <summary>
    /// Information storage for communication between the Bluetooth host application and the Unity application
    /// </summary>
    [Serializable]
    public class CommunicationObject
    {
        /// <summary>
        /// Indication of what action the server should do
        /// </summary>
        public TCPServerCommand ServerCommand =  TCPServerCommand.ArduinoCommand;

        /// <summary>
        /// ID of the Arduino board
        /// </summary>
        public int ArduinoID = -1;
        /// <summary>
        /// The specific command which is sent to the Arduino
        /// </summary>
        public CommandType BTCommandType;

        /// <summary>
        /// Parameters are needed for some specific commands
        /// </summary>
        public object[] Arguments;
    }

    /// <summary>
    /// Available commands to be sent to the BT host application
    /// </summary>
    public enum TCPServerCommand{
        /// <summary>
        /// Anything concerning general information about the Arduino devices
        /// </summary>
        AndroidInformation,
        /// <summary>
        /// Enables BT command exchange to Arduino devices
        /// </summary>
        ArduinoCommand,
        /// <summary>
        /// Connecting Arduino devices via BT
        /// </summary>
        ArduinoConnect,
        ArduinoDisconnect
    }
}
