﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace AIO
{
    /// <summary>
    /// Manages the hardware connection to the Arduino devices
    /// </summary>
    public class ArduinoManager
    {

        private Dictionary<int, ArduinoIO> arduinos = new Dictionary<int, ArduinoIO>();
        private Dictionary<string, string> BTAddressList = new Dictionary<string, string>();

        /// <summary>
        /// Raised, if any BT device was successfully connected
        /// </summary>
        public event EventHandler<InformationEventArgs> BTDeviceConnected;
        public event EventHandler<InformationEventArgs> BTConnectionInProgress;

        /// <summary>
        /// Connects all available Arduino devices via USB
        /// </summary>
        /// <returns>the number of found devices</returns>
        public int ConnectToAllSerial()
        {
            foreach (string portName in SerialPort.GetPortNames())
            {
                ArduinoIO newConnection = new ArduinoIO();
                if (newConnection.ConnectSerial(portName))
                {
                   arduinos.Add(newConnection.Id, newConnection);
                }
            }
            return arduinos.Count;
        }

        /// <summary>
        /// Connect to all available Arduino devices via BT
        /// </summary>
        /// <returns></returns>
        public int ConnectToAll()
        {
           foreach(KeyValuePair<string,string> item in BTAddressList){
               ArduinoIO newConnection = new ArduinoIO(item.Key, item.Value);
               newConnection.ConnectionEstablished += (sender, args) => {
                   arduinos.Add(newConnection.Id, newConnection);
                   if (BTDeviceConnected != null)
                       BTDeviceConnected(newConnection, args);
               };
               newConnection.ConnectionInProgress += (sender, args) =>
               {
                   if (BTConnectionInProgress != null)
                       BTConnectionInProgress(sender, args);
               };
               newConnection.Connect();
           }
           return arduinos.Count;
        }

        /// <summary>
        /// Adds an entry for a new Bluetooth device
        /// </summary>
        /// <param name="deviceAddress">BT address</param>
        /// <param name="devicePIN">BT device pin, default is 1234</param>
        public void AddBTInformation(string deviceAddress, string devicePIN = "1234")
        {
            BTAddressList.Add(deviceAddress, devicePIN);
        }

        /// <summary>
        /// Gets the device with the given id number
        /// </summary>
        /// <param name="id">ID of device</param>
        /// <returns>the Arduino handler object</returns>
        public ArduinoIO GetDeviceById(int id)
        {
            return this.arduinos[id];
        }
    }
}
