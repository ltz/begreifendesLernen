﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.IO;
using System.Text;
using CommandMessenger;
using CommandMessenger.Transport;
using CommandMessenger.Transport.Bluetooth;
using System.Runtime.CompilerServices;

namespace AIO
{
    /// <summary>
    /// Communication interface for the Arduino UNO board
    /// </summary>
    public class ArduinoIO
    {
        internal static short RUNNING_ID = 0;
        private int id = -1;

        // Arduino via USB
        private SerialPort comPort;
        private string comPortName;

        // Arduino via Bluetooth
        private BluetoothTransport btTransport;
        private BluetoothConnectionManager btConMangr;
        private CmdMessenger cmdMsgr;


        private const int BAUD_RATE = 9600; // USB Serial
        private const int BT_BAUD_RATE = 115200; // Bluetooth

        protected const int COM_LENGTH = 6;
        protected byte[] COM_INIT = new byte[] { 35, 0, 0, 0, 0, 0 };

        protected const int MAGNET_PIN1 = 8;
        protected const int MAGNET_PIN2 = 9;
        protected const int VIBRATION_PIN = 12;
        protected const int GREEN_PIN1 = 2;
        protected const int BLUE_PIN1 = 3;
        protected const int RED_PIN1 = 5;
        protected const int GREEN_PIN2 = 6;
        protected const int BLUE_PIN2 = 7;
        protected const int RED_PIN2 = 10;
        protected const int HALL_EFFECT_SENSOR_PIN = 4;

        /// <summary>
        /// Called when the Arduino board was successfully connected via BT
        /// </summary>
        public event EventHandler<InformationEventArgs> ConnectionEstablished;

        public event EventHandler<InformationEventArgs> ConnectionInProgress;

        

        /*
         * PIN ASSIGNMENT
         * DIGITAL PINS
         * 8   -   MAGNET
         * 12   -   VIBRATION MOTOR
         * 13    -   LED1
         * 7    -   LED2
         * 11    - SPEAKER
         * 4    - HALL EFFECT SENSOR
         * 
         * 6 byte COM protocol:
         * || ASCII #   | command type | value1 upper 8bit |   value1 lower 8bit  | value2 upper8 | value2 lower8 |
         * ||-----------|--------------|-------------------|----------------------|---------------|---------------|
         * ||   35      |   0-255      |                   |                      |               |               |
         * ||  8-bit u  |   8-bit u    | 8bit u            | s short              |   8bit u      | C# spec       |
         * || byte      |   byte       | byte              | int                  |   byte        | arduino spec  |
         * 
         * 
         * COM SPECIFICATION:
         * TYPE         |     DESCRIPTION
         * ####### 0-10 connection commands       ######
         * || ASCII #   | 0-10         | value1     |   value2  |
         * ||-----------|--------------|------------|-----------|
         * ||   35      |   0-255      | 0-255      |           |
         * ||  8-bit u  |   8-bit u    | s short    | s short   |
         * || byte      |   byte       | int       | int       |
         * 0            |     INIT: identification request
         * 1            |     INIT: value1 = OK, ER, OT
         * ####### 50-99 single module commands ######
         * || ASCII #   | 10-50        | pin number                 |   value       | additional value  |
         * ||-----------|--------------|----------------------------|---------------|-------------------|
         * ||   35      |   0-255      | 1-13 digital, 21-25 analog |               |                   |
         * ||  8-bit u  |   8-bit u    |  8bit u                    | s short       |  8bit u           |
         * || byte      |   byte       |  byte                      | int           |  byte             |
         * 51           |     LED blink delay in ms, < 0 = off
         * 52           |     MAGNET_ONOFF
         * 53           |     BUTTON_PRESSING 1/0
         * 54           |     VIBRATOR_ONOFF 1/0 or pulsing interval in ms
         * 55           |     HALL_SENSOR
         * 56           |     SPEAKER
         * ####### 100 state transition #######
         * || ASCII #   |   100        | bit state  |   value   | value2 lower8    |
         * ||-----------|--------------|------------|-----------|------------------|
         * ||   35      |   100        | state      |           |  state ID        |
         * ||  8-bit u  |   8-bit u    | 8bit u     | s short   | s short          |
         * || byte      |   byte       | byte       | int       |   int            |
         * 
         * 
         * bit state:
         * |   MSB      |   6                |   5       |   4           |   3     |   2       |   1   |   0   |
         * |  magnet    |  vibration motor   |  speaker  |  hall sensor  |  LED1   |   LED2    | free bits     |
         * 
         * */

        /// <summary>
        /// A new instance with given Bluetooth information
        /// </summary>
        /// <param name="BTAddress">BT device address</param>
        /// <param name="BTPIN">BT pin</param>
        public ArduinoIO(string BTAddress, string BTPIN)
        {
            btTransport = new BluetoothTransport()
            {
                CurrentBluetoothDeviceInfo = BluetoothUtils.DeviceByAdress(BTAddress)
            };
            cmdMsgr = new CmdMessenger(btTransport, BoardType.Bit16); // Arduino UNO

            attachCallbacks();
            btConMangr = new BluetoothConnectionManager(btTransport, cmdMsgr)
            {
                //WatchdogEnabled = true,
                DevicePins = {
                        {BTAddress, BTPIN}
                }
            };

            btConMangr.ConnectionFound += btConMangr_ConnectionFound;
            btConMangr.ConnectionTimeout += btConMangr_ConnectionTimeout;
            btConMangr.Progress += btConMangr_Progress;
        }

        /// <summary>
        /// Functional callbacks
        /// </summary>
        private void attachCallbacks(){
            cmdMsgr.Attach((int)CommandType.kIdentify, BoardIdentification);
            //cmdMsgr.Attach((int)CommandType.kLED1Control, LED1Control);
        }

        /// <summary>
        /// Callback for the identification phase
        /// </summary>
        /// <param name="response"></param>
        private void BoardIdentification(ReceivedCommand response){
            if (response.Ok) // check if response is valid
            {
                id = int.Parse(response.Arguments[0]);
                if (ConnectionEstablished != null)
                    ConnectionEstablished(this, new InformationEventArgs(response.Arguments[0]));
                Console.WriteLine(">> connection successful for " + btTransport.CurrentBluetoothDeviceInfo.DeviceAddress);
            }
        }

        internal ArduinoIO()
        {

        }

        void btConMangr_Progress(object sender, ConnectionManagerProgressEventArgs e)
        {
            if (ConnectionInProgress != null)
                ConnectionInProgress(this, new InformationEventArgs(e.Description));
        }

        void btConMangr_ConnectionTimeout(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void btConMangr_ConnectionFound(object sender, EventArgs e)
        {
            // identify arduino board
            SendCommand identificationCmd = new SendCommand((int)CommandType.kIdentify);
            
            cmdMsgr.SendCommand(identificationCmd);
        }

      

        /// <summary>
        /// Tries to establish a connection to an Arduino board connected on any USB port
        /// </summary>
        /// 
        public bool ConnectSerial()
        {
            if (comPort == null)
            {
                // try to detect an arduino board
                foreach (string portName in SerialPort.GetPortNames())
                {
                    if (ConnectSerial(portName))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Tries to establish a BT connection
        /// </summary>
        public void Connect()
        {
            btConMangr.StartConnectionManager();
        }

        /// <summary>
        /// Goes through all COM ports available and tries to detect an Arduino device
        /// </summary>
        /// <param name="p">serial port object</param>
        /// <returns>true, if connection established</returns>
        private bool DetectSerial(SerialPort p)
        {
            byte[] comAnswer = new byte[COM_LENGTH];
            try
            {
                p.Open();

                //configure COM_INT
                COM_INIT[3] = (byte) (RUNNING_ID >> 8);
                COM_INIT[4] = (byte)(RUNNING_ID & 0xf);
                RUNNING_ID++; // new arduino board

                // send initialization sequence
                p.Write(COM_INIT, 0, COM_LENGTH);

                // read answer
                for (int i = 0; i < COM_LENGTH; i++)
                {
                    comAnswer[i] = (byte)p.ReadByte();
                }

                string response = ((char)comAnswer[2]).ToString();
                response += (char)comAnswer[3];

                if (response == "OK")
                {
                    id = ((comAnswer[4] << 8) | comAnswer[5]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                // probably already in use
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// Tries to establish a connection to an Arduino board on a given port name
        /// </summary>
        /// <param name="portName">name of the port</param>
        ///
        public bool ConnectSerial(string portName)
        {
            SerialPort p = new SerialPort(portName, BAUD_RATE);
            if (DetectSerial(p))
            {
                this.comPort = p;
                this.comPortName = portName;
                return true;
            }
            return false;
        }

        public ReceivedCommand SendCommandBT(CommandType ct, params object[] cmdParams)
        {
            return SendCommandResponseBT(ct, ct, cmdParams);
        }

        
        internal ReceivedCommand SendCommandResponseBT(CommandType ct, CommandType respCt, params object[] cmdParams)
        {
            SendCommand cmd = (respCt == ct) ? new SendCommand((int)ct) : new SendCommand((int)ct, (int)respCt, 500);
            if (cmdParams.Length > 0)
            {
                for (int i = 0; i < cmdParams.Length; i++)
                {
                    object currentParam = cmdParams[i];
                    if (currentParam is int)
                    {
                        cmd.AddArgument((int)currentParam);
                    }
                }
            }
            return cmdMsgr.SendCommand(cmd);
        }

        /// <summary>
        /// Sends a command to a single module on the device
        /// </summary>
        /// <remarks>This method is synchronous.</remarks>
        /// <param name="ct"> type of command to send</param>
        /// <param name="value"> main value sent to module</param>
        /// <param name="optVal"> optional value, default is 0. Must use for CommandType.LED_CONTROL and MAGNET_CONTROL</param>
        /// <returns> the value from the device or -1, if there was a command error</returns>
        public short SendCommandSerial(CommandType ct, short value, byte optVal = 0)
        {

            byte[] dataReq = new byte[COM_LENGTH];
            dataReq[0] = 35;
            dataReq[1] = (byte)ct;

            //determine the pin
            if (ct == CommandType.kMagnetControl) {
                switch ((LEDControls)optVal)
                {
                    case LEDControls.LED1_BLUE:
                        dataReq[2] = BLUE_PIN1;
                        break;
                    case LEDControls.LED1_GREEN:
                        dataReq[2] = GREEN_PIN1;
                        break;
                    case LEDControls.LED1_RED:
                        dataReq[2] = RED_PIN1;
                        break;
                    case LEDControls.LED2_BLUE:
                        dataReq[2] = BLUE_PIN2;
                        break;
                    case LEDControls.LED2_GREEN:
                        dataReq[2] = GREEN_PIN2;
                        break;
                    case LEDControls.LED2_RED:
                        dataReq[2] = RED_PIN2;
                        break;
                }
            }
            else if (ct == CommandType.kMagnetControl) {
                dataReq[2] = (byte)((optVal == 1) ? MAGNET_PIN1 : MAGNET_PIN2);
            }
            else if (ct == CommandType.kVibrationControl) { dataReq[2] = VIBRATION_PIN; }
            else { return -1; }

            // set value sent to pin
            dataReq[3] = (byte)(value >> 8); // get upper 8 bits of value
            dataReq[4] = (byte)(value & 0xff); // get lower 8 bits of value
            dataReq[5] = optVal;

            return SendRaw(dataReq);
        }

        ///
        /// <summary>
        /// Sends a command for a state transition
        /// </summary>
        /// <remarks>
        /// This method is synchronous
        /// </remarks>
        /// <param name="ct">type of command to send</param>
        /// <param name="value">main value sent to module</param>
        /// <param name="optVal">optional value, default is 0</param>
        /// <returns> the value from the device or -1, if there was a command error</returns>
        public short ChangeStateSerial(CommandType ct, byte newState, short value)
        {

            byte[] dataReq = new byte[COM_LENGTH];
            dataReq[0] = 35;
            dataReq[1] = (byte)ct;

            // set new transition state
            if ((int)ct == 999) { dataReq[2] = newState; }

            // set value
            dataReq[3] = (byte)(value >> 8); // get upper 8 bits of value
            dataReq[4] = (byte)(value & 0xff); // get lower 8 bits of value

            dataReq[5] = 0;
            return SendRaw(dataReq);
        }

        /// <summary>
        /// Waiting for any reply from the connected device
        /// </summary>
        /// <remarks>
        /// This method is synchronous and will block the program until a response is received.
        /// See <see cref="WaitReplyAsync"/> for an asychronomus mechanism.
        /// </remarks>
        /// <returns>a value from the device</returns>
        public short WaitReplySerial()
        {
            byte[] dataResp = new byte[COM_LENGTH];
            if (!this.comPort.IsOpen)
            {
                this.comPort.Open();
            }
            for (int i = 0; i < COM_LENGTH; i++)
            {
                dataResp[i] = (byte)this.comPort.ReadByte();
            }
            return (short)(dataResp[3] << 8 | dataResp[4]);

        }

        ///// <summary>
        ///// Waits asynchronously for a reply from the device
        ///// </summary>
        ///// <returns>a value sent by that device</returns>
        //public async Task<short> WaitReplyAsync()
        //{
        //    byte[] dataResp = new byte[COM_LENGTH];
        //    int readCount = await this.comPort.BaseStream.ReadAsync(dataResp, 0, COM_LENGTH);
        //    return (short)(dataResp[3] << 8 | dataResp[4]);
        //}

        /// <summary>
        /// Sends raw data from a 6byte buffer and waits for a reply (synchronous)
        /// </summary>
        protected short SendRaw(byte[] data)
        {
            if (data.Length == 6)
            {
                if (!this.comPort.IsOpen)
                {
                    this.comPort.Open();
                }
                this.comPort.Write(data, 0, COM_LENGTH);
            }
            return WaitReplySerial();
        }

        /// <summary>
        /// Closes the connection to the port and disposes of the object
        /// </summary>
        public void Disconnect()
        {
            this.comPort.Close();
            this.comPort.Dispose();
        }

        /// <summary>
        /// Closes the connection to the port
        /// </summary>
        public void Close()
        {
            this.comPort.Close();
        }

        /// <summary>
        /// Generates the bit pattern for a particular state on the device
        /// </summary>
        /// <param name="magnet1"> true, if the first magnet will be on</param>
        /// <param name="magnet2"> true, if the second magnet will be on</param>
        /// <param name="vibration"> true, if the vibration motor will be on</param>
        /// <param name="led1Control"> status of the first RGB led
        /// <param name="led2Control"> status of the second RGB led
        /// <returns> single byte with the correct bit pattern</returns>
        public static byte GenerateState(bool magnet1, bool magnet2, bool vibration, LEDControls led1Control, LEDControls led2Control)
        {
            byte newState = 0;
            newState = (magnet1) ? (byte)(newState | (1 << 7)) : newState;
            newState = (magnet2) ? (byte)(newState | (1 << 6)) : newState;
            newState = (vibration) ? (byte)(newState | (1 << 5)) : newState;
            
            // controlling LED
            switch(led1Control){
            	case LEDControls.LED1_OFF:
            		newState = (byte)(newState | (0 << 4));
            		newState = (byte)(newState | (0 << 3));
            		break;
            	case LEDControls.LED1_RED:
            		newState = (byte)(newState | (0 << 4));
            		newState = (byte)(newState | (1 << 3));
            		break;
            	case LEDControls.LED1_GREEN:
            		newState = (byte)(newState | (1 << 4));
            		newState = (byte)(newState | (0 << 3));
            		break;
            	case LEDControls.LED1_BLUE:
            		newState = (byte)(newState | (1 << 4));
            		newState = (byte)(newState | (1 << 3));
            		break;
            }

            switch(led2Control){
            	case LEDControls.LED2_OFF:
            		newState = (byte)(newState | (0 << 2));
            		newState = (byte)(newState | (0 << 1));
            		break;
            	case LEDControls.LED2_RED:
            		newState = (byte)(newState | (0 << 2));
            		newState = (byte)(newState | (1 << 1));
            		break;
            	case LEDControls.LED2_GREEN:
            		newState = (byte)(newState | (1 << 2));
            		newState = (byte)(newState | (0 << 1));
            		break;
            	case LEDControls.LED2_BLUE:
            		newState = (byte)(newState | (1 << 2));
            		newState = (byte)(newState | (1 << 1));
            		break;
            }
           	newState = (byte)(newState | (0 << 0));
            return newState;
        }

        /// <summary>
        /// Gets the id of this particular device
        /// </summary>
        /// <returns>ID number</returns>
        public int Id
        {
            get{
                return id;
            }
        }
    }
    /// <summary>
    /// List of different commands for an Arduino device
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// Bluetooth Identification 
        /// </summary>
        kIdentify,
        /// <summary>
        /// OK command
        /// </summary>
        kOK, 
        /// <summary>
        /// ERROR command
        /// </summary>
        kERR,
        /// <summary>
        /// Command for controlling first RGB led
        /// </summary>
        kLED1Control,
        /// <summary>
        /// Command for controlling second RGB led
        /// </summary>
        kLED2Control,
        /// <summary>
        /// Command for controlling vibration motor
        /// </summary>
        kVibrationControl,
        /// <summary>
        /// Command for controlling first magnet
        /// </summary>
        kMagnetControl
    };

    /// <summary>
    /// Status commands for the LEDs on the device
    /// </summary>
    public enum LEDControls
    {
    	LED1_OFF = 0,
    	LED1_RED = 1,
    	LED1_GREEN = 2,
    	LED1_BLUE = 3,
    	LED2_OFF = 10,
    	LED2_RED = 11,
    	LED2_GREEN = 12,
    	LED2_BLUE = 13
    }

    public class InformationEventArgs : EventArgs
    {
        private string information;
        public InformationEventArgs(string information)
        {
            this.information = information;
        }

        public string Info
        {
            get {
                return this.information;
            }
        }
    }

    /// <summary>
    /// Contains event data for a command received from an Arduino device
    /// </summary>
    public class ReceivedCommandEventArgs : EventArgs{
        private CommunicationObject comObj;
        public ReceivedCommandEventArgs(CommunicationObject comO){
            this.comObj = comO;
        }

        /// <summary>
        /// Communication information storage
        /// </summary>
        public CommunicationObject Command{
            get{
                return this.comObj;
            }
        }
    }
}
