﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Text;
using System.IO;
using System.Linq;

namespace AIO
{
    /// <summary>
    /// Manages the communication between the Bluetooth host and Unity application
    /// </summary>
    public class CommunicationManager
    {
        //Communication Sockets
        private TcpClient clientSocket;
        private NetworkStream ns;
        private static BinaryFormatter bf = new BinaryFormatter();
        private Thread comThread;
        private List<int> arduinoList;

        /// <summary>
        /// Raised, when any command was received via BT
        /// </summary>
        public event EventHandler<ReceivedCommandEventArgs> CommandReceived;
        
        /// <summary>
        /// Create a default communication manager
        /// </summary>
        public CommunicationManager()
        {
            clientSocket = new TcpClient();
            arduinoList = new List<int>();
            comThread = new Thread(new ThreadStart(ReceiveSocketMessages));
        }

        /// <summary>
        /// Sends the signal to try to connect all available BT devices
        /// </summary>
        public void ConnectToAll()
        {
            clientSocket.Connect("127.0.0.1", 8888);
            ns = clientSocket.GetStream();
            SendViaSocket(new CommunicationObject() { 
                ServerCommand = TCPServerCommand.ArduinoConnect
            });
            comThread.Start();
        }


        /// <summary>
        /// Sending information to the BT host application via TCP socket
        /// </summary>
        /// <param name="comObject">information storage object</param>
        internal void SendViaSocket(CommunicationObject comObject)
        {
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, comObject);
                byte[] sendBytes = ms.ToArray();
                ns.Write(sendBytes, 0, sendBytes.Length);
                ns.Flush();
            }
            Console.WriteLine(" >> sent package to TCP server");
        }

        /// <summary>
        /// Sends a command to a given Arduino device via the BT host
        /// </summary>
        /// <param name="arduinoId">a valid Arduino board ID</param>
        /// <param name="ct">the type of command to be executed</param>
        /// <param name="cmdParams">optional parameters for the command</param>
        public void SendCommand(int arduinoId, CommandType ct, params object[] cmdParams)
        {
            CommunicationObject sendingCommand = new CommunicationObject();
            sendingCommand.ArduinoID = arduinoId;
            sendingCommand.BTCommandType = ct;
            sendingCommand.Arguments = cmdParams;
            SendViaSocket(sendingCommand);
        }

        /// <summary>
        /// Receiving service loop
        /// </summary>
        void ReceiveSocketMessages()
        {
            while (true)
            {
                // check if data is available in pipe
                if (ns.DataAvailable)
                {
                    byte[] bytesfrom = new byte[1024];
                    ns.Read(bytesfrom, 0, clientSocket.Available);
                    Console.WriteLine(" >> read data from bt server");
                    CommunicationObject obj;
                    bool readyToFireEvent = false;

                    // check if byte stream is not all 0
                    if (bytesfrom.Any(item => item > 0))
                    {
                        using (var memstream = new MemoryStream())
                        {

                            // unmarshalling RPC object
                            memstream.Write(bytesfrom, 0, bytesfrom.Length);
                            memstream.Seek(0, SeekOrigin.Begin);
                            obj = (CommunicationObject)bf.Deserialize(memstream);
                            if (obj.ServerCommand == TCPServerCommand.AndroidInformation)
                            {
                                if (obj.ArduinoID != -1 && !arduinoList.Contains(obj.ArduinoID))
                                {
                                    // add new arduino device
                                    arduinoList.Add(obj.ArduinoID);
                                    readyToFireEvent = true;
                                }
                            }
                            else if (obj.ServerCommand == TCPServerCommand.ArduinoCommand)
                            {
                                readyToFireEvent = true;
                            }

                            // fire external event
                            if (readyToFireEvent && CommandReceived != null)
                                CommandReceived(this, new ReceivedCommandEventArgs(obj));
                        }
                    }
                    else
                    {
                        Console.WriteLine(" >> empty bytes");
                    }
                }
            }
        }
    }
}
