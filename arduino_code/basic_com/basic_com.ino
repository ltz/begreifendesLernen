#include <CmdMessenger.h>

// DECLARE GENERALS
const int magnetPin1 = 3;
const int magnetPin2 = 4;
const int motorPin = 11;
const int greenPin1 = 9;
const int bluePin1 = 8;
const int redPin1 = 10;
const int greenPin2 = 6;
const int bluePin2 = 5;
const int redPin2 = 7;
const long baudRate = 115200; //set baud rate to 115200

int led1[] = {redPin1, greenPin1, bluePin1};
int led2[] = {redPin2, greenPin2, bluePin2};

int currentLED1 = -1;
int currentLED2 = -1;

// COMPONENT PARAMETERS
int blinkLED_MS = -1;
int blinkLED2_MS = -1;
int blinkVIB_MS = -1;

// STATE PARAMS
int boardId = 50; // -1 not identified, DARK BLUE 0-10, ORANGE 50-60
int internalState = 0;

// COM BUFFERS
byte readMsg[6];
byte writeMsg[6];

CmdMessenger messenger = CmdMessenger(Serial);

enum{
  kIdentify,
  kOK,
  kERR,
  kLED1Control,
  kLED2Control,
  kVibrationControl,
  kMagnetControl
};


void attachCmdCallbacks(){
  messenger.attach(onUnknownCmd);
  messenger.attach(kIdentify, onConnectionIdentify);
  messenger.attach(kLED1Control, onLED1Control);
  messenger.attach(kLED2Control, onLED2Control);
  messenger.attach(kVibrationControl, onVibControl);
  messenger.attach(kMagnetControl, onMagnetControl);
}

void setup() {
   // setting pin modes
   pinMode(greenPin1, OUTPUT);
   pinMode(bluePin1, OUTPUT);
   pinMode(redPin1, OUTPUT);
   pinMode(greenPin2, OUTPUT);
   pinMode(bluePin2, OUTPUT);
   pinMode(redPin2, OUTPUT);
   pinMode(magnetPin1, OUTPUT);
   pinMode(magnetPin2, OUTPUT);
   pinMode(motorPin, OUTPUT);

   // attach interrupt method to button
//   attachInterrupt(digitalPinToInterrupt(hallsensorPin), executeHall, CHANGE);
   
   attachCmdCallbacks();
   Serial.begin(baudRate); 
}

void loop() {
// get BT com data
  messenger.feedinSerialData();

  // execute generic repeated tasks
  execute();

  
  /*
  if (Serial.available()){
    readMsg[0] = Serial.read();
    delay(40);
    readMsg[1] = Serial.read();
    delay(40);
    readMsg[2] = Serial.read();
    delay(40);
    readMsg[3] = Serial.read();
    delay(40);
    readMsg[4] = Serial.read();
    delay(40);
    readMsg[5] = Serial.read();
    delay(40);
  }

  // protocol processing
  if(readMsg[0] == 35){   // PROTOCOL BEGIN
    char resp[3] = "OK";
    int moduleValue = (readMsg[3] << 8) | readMsg[4]; // extract value of a module command

    writeMsg[0] = 35;
    writeMsg[1] = readMsg[1];
    switch(readMsg[1]){ // recognize type of command
      case 0:
          // identification request
          boardId = moduleValue; // set board id
          writeMsg[1] = 1;
          writeMsg[2] = resp[0];
          writeMsg[3] = resp[1];
          writeMsg[4] = readMsg[3];
          writeMsg[5] = readMsg[4];
          break;
       case 51:
          // LED control
          blinkLED_MS = moduleValue;
          if (moduleValue <= 0){
            digitalWrite(readMsg[2], LOW);
          }else if(moduleValue == 1){
            digitalWrite(readMsg[2], HIGH);
          }
          writeMsg[2] = readMsg[2];
          writeMsg[3] = 0;
          writeMsg[4] = 1;
          writeMsg[5] = 0;
          break;

       case 52:
          if (moduleValue == 1){
            digitalWrite(readMsg[2], HIGH);
          }else{
            digitalWrite(readMsg[2], LOW);
          }
          writeMsg[2] = readMsg[2];
          writeMsg[3] = 0;
          writeMsg[4] = (byte)moduleValue;
          writeMsg[5] = 0;
          break;
       case 54:
          blinkVIB_MS = moduleValue;
          if(moduleValue <= 0){
            digitalWrite(motorPin, LOW);
          }else if(moduleValue == 1){
            digitalWrite(motorPin, HIGH);
          }
          writeMsg[2] = motorPin;
          writeMsg[3] = 0;
          writeMsg[4] = 1;
          writeMsg[5] = 0;
          break;
        case 100:
          // state transition
          byte newState = readMsg[2];
          int k = 0;

          // return the old state
          writeMsg[3] = internalState >> 8;
          writeMsg[4] = internalState & 0xff;

          internalState = moduleValue;

          // apply hardware config
          digitalWrite(magnetPin1, (newState & (1 << 7))); //magnet1
          digitalWrite(magnetPin1, (newState & (1 << 6))); //magnet2
          digitalWrite(motorPin, (newState & (1 << 5))); //vib

          // control RGB leds
          int led1State = ((newState & (1 << 4)) << 8) | (newState & (1 << 3));
          int led2State = ((newState & (1 << 2)) << 8) | (newState & (1 << 1));
          if (led1State == 0){
              // all off
              for(k = 0; k < 3; k++){
                  digitalWrite(led1[k], LOW);
              }
              currentLED1 = -1;
          }else{
              for(k = 0; k < 3; k++){
                  digitalWrite(led1[k], (k == led1State) ? HIGH : LOW);
              }
              currentLED1 = led1[led1State]; // get current high led1 pin
          }

          if (led2State == 0){
              // all off
              for(k = 0; k < 3; k++){
                  digitalWrite(led2[k], LOW);
              }
              currentLED2 = -1;
          }else{
              for(k = 0; k < 3; k++){
                  digitalWrite(led2[k], (k == led2State) ? HIGH : LOW);
              }
              currentLED2 = led2[led2State]; // get current high led2 pin
          }
          
          writeMsg[2] = newState;
          writeMsg[5] = 0;
          break;
    }
    readMsg[0] = 0;
    readMsg[1] = 0;
    readMsg[2] = 0;
    readMsg[3] = 0;
    readMsg[4] = 0;
    readMsg[5] = 0;
    writeToOutput();
  }*/
}

void execute(){
  if (blinkLED_MS > 1){
    digitalWrite(currentLED1, HIGH);
    delay(blinkLED_MS);
    digitalWrite(currentLED1, LOW);
    delay(blinkLED_MS);
  }

  if (blinkLED2_MS > 1){
    digitalWrite(currentLED2, HIGH);
    delay(blinkLED2_MS);
    digitalWrite(currentLED2, LOW);
    delay(blinkLED2_MS);
  }

  if (blinkVIB_MS > 1){
    digitalWrite(motorPin, HIGH);
    delay(blinkVIB_MS);
    digitalWrite(motorPin, LOW);
    delay(blinkVIB_MS);
  }
}

void executeHall(){
    // read Hall sensor
  
  /*if (digitalRead(BUTTON_PIN)){
        digitalWrite(motorPin, HIGH);
        digitalWrite(ledPin, HIGH);
        writeMsg[4] = 1;
        startPlayback(sample, sizeof(sample));
  }else{
      digitalWrite(motorPin, LOW);
      digitalWrite(ledPin, LOW);
        writeMsg[4] = 0;
        stopPlayback;
  }*/

 /* if (digitalRead(hallsensorPin)){
    writeMsg[0] = 35;
    writeMsg[1] = 55;
    writeMsg[2] = hallsensorPin;
    writeMsg[3] = 0;
    writeMsg[4] = 1;
    writeMsg[5] = 0;
    writeToOutput();

    // debug purposes
    digitalWrite(redPin1, HIGH);
    delay(200);
    digitalWrite(redPin1, LOW);
  }*/
}

void writeToOutput(){
  Serial.write(writeMsg[0]);
  Serial.write(writeMsg[1]);
  Serial.write(writeMsg[2]);
  Serial.write(writeMsg[3]);
  Serial.write(writeMsg[4]);
  Serial.write(writeMsg[5]);
  writeMsg[0] = 0;
  writeMsg[1] = 0;
  writeMsg[2] = 0;
  writeMsg[3] = 0;
  writeMsg[4] = 0;
  writeMsg[5] = 0;
}

// CALLBACKS

void onUnknownCmd(){
  
}

void onConnectionIdentify(){
  messenger.sendCmd(kIdentify, boardId);
}

void onLED1Control(){
   int choiceIndex = messenger.readInt16Arg();
   blinkLED_MS = messenger.readInt16Arg();
   if (blinkLED_MS <= 0){
      currentLED1 = -1;
      digitalWrite(led1[choiceIndex], LOW);
   }
   else{
      currentLED1 = led1[choiceIndex];
      digitalWrite(currentLED1, HIGH);
   }
   messenger.sendCmd(kOK);
}

void onLED2Control(){
   int choiceIndex = messenger.readInt16Arg();
   blinkLED2_MS = messenger.readInt16Arg();
   if (blinkLED2_MS <= 0){
      currentLED2 = -1;
      digitalWrite(led2[choiceIndex], LOW);
   }
   else{
      currentLED2 = led2[choiceIndex];
      digitalWrite(currentLED2, HIGH);
   }
   messenger.sendCmd(kOK);
}

void onVibControl(){
   blinkVIB_MS = messenger.readInt16Arg();
   if (blinkVIB_MS <= 0){
      digitalWrite(motorPin, LOW);
   }
   else{
      digitalWrite(motorPin, HIGH);
   }
   messenger.sendCmd(kOK);
}

void onMagnetControl(){

}
