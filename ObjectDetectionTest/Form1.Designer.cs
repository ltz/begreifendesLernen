﻿namespace ObjectDetectionTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKinectStatDesc = new System.Windows.Forms.Label();
            this.lblKinectStat = new System.Windows.Forms.Label();
            this.outputImg = new System.Windows.Forms.Panel();
            this.tbHueH = new System.Windows.Forms.TrackBar();
            this.tbSaturationH = new System.Windows.Forms.TrackBar();
            this.tbValueH = new System.Windows.Forms.TrackBar();
            this.textBoxHueH = new System.Windows.Forms.TextBox();
            this.textBoxValueH = new System.Windows.Forms.TextBox();
            this.textBoxSaturationH = new System.Windows.Forms.TextBox();
            this.tbHueL = new System.Windows.Forms.TrackBar();
            this.tbSaturationL = new System.Windows.Forms.TrackBar();
            this.tbValueL = new System.Windows.Forms.TrackBar();
            this.textBoxHueL = new System.Windows.Forms.TextBox();
            this.textBoxSaturationL = new System.Windows.Forms.TextBox();
            this.textBoxValueL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.outputColorImg = new System.Windows.Forms.Panel();
            this.button_detect = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbHueH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSaturationH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbValueH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHueL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSaturationL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbValueL)).BeginInit();
            this.SuspendLayout();
            // 
            // lblKinectStatDesc
            // 
            this.lblKinectStatDesc.AutoSize = true;
            this.lblKinectStatDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKinectStatDesc.Location = new System.Drawing.Point(104, 11);
            this.lblKinectStatDesc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKinectStatDesc.Name = "lblKinectStatDesc";
            this.lblKinectStatDesc.Size = new System.Drawing.Size(119, 20);
            this.lblKinectStatDesc.TabIndex = 0;
            this.lblKinectStatDesc.Text = "Camera status";
            // 
            // lblKinectStat
            // 
            this.lblKinectStat.AutoSize = true;
            this.lblKinectStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKinectStat.Location = new System.Drawing.Point(307, 11);
            this.lblKinectStat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKinectStat.Name = "lblKinectStat";
            this.lblKinectStat.Size = new System.Drawing.Size(100, 20);
            this.lblKinectStat.TabIndex = 1;
            this.lblKinectStat.Text = "not connect.";
            // 
            // outputImg
            // 
            this.outputImg.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.outputImg.Location = new System.Drawing.Point(22, 12);
            this.outputImg.Name = "outputImg";
            this.outputImg.Size = new System.Drawing.Size(581, 305);
            this.outputImg.TabIndex = 2;
            // 
            // tbHueH
            // 
            this.tbHueH.Location = new System.Drawing.Point(776, 70);
            this.tbHueH.Maximum = 255;
            this.tbHueH.Name = "tbHueH";
            this.tbHueH.Size = new System.Drawing.Size(266, 56);
            this.tbHueH.TabIndex = 3;
            this.tbHueH.Value = 100;
            this.tbHueH.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // tbSaturationH
            // 
            this.tbSaturationH.Location = new System.Drawing.Point(776, 231);
            this.tbSaturationH.Maximum = 255;
            this.tbSaturationH.Name = "tbSaturationH";
            this.tbSaturationH.Size = new System.Drawing.Size(266, 56);
            this.tbSaturationH.TabIndex = 4;
            this.tbSaturationH.Value = 100;
            this.tbSaturationH.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // tbValueH
            // 
            this.tbValueH.Location = new System.Drawing.Point(776, 386);
            this.tbValueH.Maximum = 255;
            this.tbValueH.Name = "tbValueH";
            this.tbValueH.Size = new System.Drawing.Size(266, 56);
            this.tbValueH.TabIndex = 5;
            this.tbValueH.Value = 100;
            this.tbValueH.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // textBoxHueH
            // 
            this.textBoxHueH.Location = new System.Drawing.Point(1048, 70);
            this.textBoxHueH.Name = "textBoxHueH";
            this.textBoxHueH.Size = new System.Drawing.Size(72, 26);
            this.textBoxHueH.TabIndex = 6;
            // 
            // textBoxValueH
            // 
            this.textBoxValueH.Location = new System.Drawing.Point(1048, 386);
            this.textBoxValueH.Name = "textBoxValueH";
            this.textBoxValueH.Size = new System.Drawing.Size(72, 26);
            this.textBoxValueH.TabIndex = 7;
            // 
            // textBoxSaturationH
            // 
            this.textBoxSaturationH.Location = new System.Drawing.Point(1048, 231);
            this.textBoxSaturationH.Name = "textBoxSaturationH";
            this.textBoxSaturationH.Size = new System.Drawing.Size(72, 26);
            this.textBoxSaturationH.TabIndex = 8;
            // 
            // tbHueL
            // 
            this.tbHueL.Location = new System.Drawing.Point(776, 141);
            this.tbHueL.Maximum = 255;
            this.tbHueL.Name = "tbHueL";
            this.tbHueL.Size = new System.Drawing.Size(266, 56);
            this.tbHueL.TabIndex = 9;
            this.tbHueL.Value = 100;
            this.tbHueL.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // tbSaturationL
            // 
            this.tbSaturationL.Location = new System.Drawing.Point(776, 293);
            this.tbSaturationL.Maximum = 255;
            this.tbSaturationL.Name = "tbSaturationL";
            this.tbSaturationL.Size = new System.Drawing.Size(266, 56);
            this.tbSaturationL.TabIndex = 10;
            this.tbSaturationL.Value = 100;
            this.tbSaturationL.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // tbValueL
            // 
            this.tbValueL.Location = new System.Drawing.Point(776, 448);
            this.tbValueL.Maximum = 255;
            this.tbValueL.Name = "tbValueL";
            this.tbValueL.Size = new System.Drawing.Size(266, 56);
            this.tbValueL.TabIndex = 11;
            this.tbValueL.Value = 100;
            this.tbValueL.ValueChanged += new System.EventHandler(this.tbBlue_ValueChanged);
            // 
            // textBoxHueL
            // 
            this.textBoxHueL.Location = new System.Drawing.Point(1048, 141);
            this.textBoxHueL.Name = "textBoxHueL";
            this.textBoxHueL.Size = new System.Drawing.Size(72, 26);
            this.textBoxHueL.TabIndex = 12;
            // 
            // textBoxSaturationL
            // 
            this.textBoxSaturationL.Location = new System.Drawing.Point(1048, 294);
            this.textBoxSaturationL.Name = "textBoxSaturationL";
            this.textBoxSaturationL.Size = new System.Drawing.Size(72, 26);
            this.textBoxSaturationL.TabIndex = 13;
            // 
            // textBoxValueL
            // 
            this.textBoxValueL.Location = new System.Drawing.Point(1048, 448);
            this.textBoxValueL.Name = "textBoxValueL";
            this.textBoxValueL.Size = new System.Drawing.Size(72, 26);
            this.textBoxValueL.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(704, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "HigherH";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(704, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 16;
            this.label2.Text = "HigherS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(704, 386);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "HigherV";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(704, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "LowerH";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(704, 294);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "LowerS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(704, 448);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 20);
            this.label6.TabIndex = 20;
            this.label6.Text = "LowerV";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(726, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "0";
            // 
            // outputColorImg
            // 
            this.outputColorImg.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.outputColorImg.Location = new System.Drawing.Point(22, 323);
            this.outputColorImg.Name = "outputColorImg";
            this.outputColorImg.Size = new System.Drawing.Size(581, 334);
            this.outputColorImg.TabIndex = 22;
            // 
            // button_detect
            // 
            this.button_detect.Location = new System.Drawing.Point(1004, 529);
            this.button_detect.Name = "button_detect";
            this.button_detect.Size = new System.Drawing.Size(102, 46);
            this.button_detect.TabIndex = 23;
            this.button_detect.Text = "Detect";
            this.button_detect.UseVisualStyleBackColor = true;
            this.button_detect.Click += new System.EventHandler(this.button_detect_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(609, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 20);
            this.label8.TabIndex = 24;
            this.label8.Text = "BlobCount";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(681, 510);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(274, 83);
            this.textBox1.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1136, 669);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button_detect);
            this.Controls.Add(this.outputColorImg);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxValueL);
            this.Controls.Add(this.textBoxSaturationL);
            this.Controls.Add(this.textBoxHueL);
            this.Controls.Add(this.tbValueL);
            this.Controls.Add(this.tbSaturationL);
            this.Controls.Add(this.tbHueL);
            this.Controls.Add(this.textBoxSaturationH);
            this.Controls.Add(this.textBoxValueH);
            this.Controls.Add(this.textBoxHueH);
            this.Controls.Add(this.tbValueH);
            this.Controls.Add(this.tbSaturationH);
            this.Controls.Add(this.tbHueH);
            this.Controls.Add(this.outputImg);
            this.Controls.Add(this.lblKinectStat);
            this.Controls.Add(this.lblKinectStatDesc);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Camera object detection";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbHueH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSaturationH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbValueH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHueL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSaturationL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbValueL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKinectStatDesc;
        private System.Windows.Forms.Label lblKinectStat;
        private System.Windows.Forms.Panel outputImg;
        private System.Windows.Forms.TrackBar tbHueH;
        private System.Windows.Forms.TrackBar tbSaturationH;
        private System.Windows.Forms.TrackBar tbValueH;
        private System.Windows.Forms.TextBox textBoxHueH;
        private System.Windows.Forms.TextBox textBoxValueH;
        private System.Windows.Forms.TextBox textBoxSaturationH;
        private System.Windows.Forms.TrackBar tbHueL;
        private System.Windows.Forms.TrackBar tbSaturationL;
        private System.Windows.Forms.TrackBar tbValueL;
        private System.Windows.Forms.TextBox textBoxHueL;
        private System.Windows.Forms.TextBox textBoxSaturationL;
        private System.Windows.Forms.TextBox textBoxValueL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel outputColorImg;
        private System.Windows.Forms.Button button_detect;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox1;
    }
}

