﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CamIO;
namespace ObjectDetectionTest
{
    public partial class Form1 : Form
    {
        //internal CameraManager_threshold camMgr = new CameraManager_threshold();
        internal CameraManager camMgr = new CameraManager();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (camMgr.connect())
            {
                lblKinectStat.Text = "connected!";
                camMgr.FrameChanged += kinect_FrameChanged;
                camMgr.ObjectsMoved += kinect_ObjectsMoved;
            }
        }

        void kinect_ObjectsMoved(object sender, PositionChangedEventArgs e)
        {
            Console.WriteLine(e.getBoundingBox());
        }

        void kinect_FrameChanged(object sender, FrameChangedEventArgs e)
        {
            outputImg.BackgroundImage = e.threshold_image;
            outputColorImg.BackgroundImage = e.color_image;
            //label7.Text = e.blobCount.ToString();
        }


        private void tbBlue_ValueChanged(object sender, EventArgs e)
        {
            camMgr.setThresholdColor(new Emgu.CV.Structure.Hsv(tbHueH.Value, tbSaturationH.Value, tbValueH.Value), new Emgu.CV.Structure.Hsv(tbHueL.Value, tbSaturationL.Value, tbSaturationL.Value));
         
            this.textBoxHueH.Text = tbHueH.Value.ToString();
            this.textBoxSaturationH.Text = tbSaturationH.Value.ToString();
            this.textBoxValueH.Text = tbValueH.Value.ToString();
            this.textBoxHueL.Text = tbHueL.Value.ToString();
            this.textBoxSaturationL.Text = tbSaturationL.Value.ToString();
            this.textBoxValueL.Text = tbValueL.Value.ToString();
        }

        private void button_detect_Click(object sender, EventArgs e)
        {
            System.Console.WriteLine("H:" + tbHueH.Value.ToString() + "," + tbHueL.Value.ToString());
            System.Console.WriteLine("S:" + tbSaturationH.Value.ToString() + "," + tbSaturationL.Value.ToString());
            System.Console.WriteLine("V:" + tbValueH.Value.ToString() + "," + tbValueL.Value.ToString());
            if (textBox1.Text != "")
            {
                String[] splitValues = textBox1.Text.Split((char)10);
                for (int i = 0; i < 3; i++)
                {
                    String[] lineValues = splitValues[i].Replace("\r", "").Split((char)44);
                    String firstVal = lineValues[0].Substring(2);
                    String secVal = lineValues[1];
                    switch (i)
                    {
                        case 0:
                            tbHueH.Value = int.Parse(firstVal);
                            tbHueL.Value = int.Parse(secVal);
                            break;
                        case 1:
                            tbSaturationH.Value = int.Parse(firstVal);
                            tbSaturationL.Value = int.Parse(secVal);
                            break;
                        case 2:
                            tbValueH.Value = int.Parse(firstVal);
                            tbValueL.Value = int.Parse(secVal);
                            break;
                    }
                }

            }
            this.camMgr.detect();
        }
    }
}
